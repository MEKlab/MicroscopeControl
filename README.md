Microscope Control in MATLAB
====================
This program was developed by Sebastian Jaramillo-Riveri.

Microscope_GUI
---------------------

This is the main interface for performing microscopy experiments from MATLAB.

### Capabilities

+   multiple-channel acquisition
+   Z-stacks
+   multiple stage positions
+   time-lapse
+   autofocus from bright-field
+   program MACS experiments (pump and valves)
+   program mother machine experiments (peristaltic pump and valves)

### TODO

+   Autofocus for multiple adquisitions
+   Drift correction
+   CMOS Camera
+   SetROI doesn't do anything.
+   Channel first option for multiple XY adquisition (e.g. with different time intervals each)
+   Is there a faster way of doing Z-stacks?

### Usage
Run:
+ preMicroscope.m
+ MicroscopeGUI.m
+ postMicroscope.m (when finished)

TODO

Snap
+   Snap will adquire an image with specifications defined by the selected "Current" row in adquision channel. If multile rows are selected it will pick the first one.

Live
+   Live will do a continous streaming with specifications defined by the selected "Current" row in adquision channel. If multile rows are selected it will pick the first one.

Autofocus
+   The editable number in the autofocus panel refer to the number of column in the Z-stack table. The same row will be used if autofocus is selected during time-lapse or multiple XY acquisitions.

Drift
+   TODO

Multiple Acquisition
+   TODO

Z program table
+   name: will be used for saving files.
+   Up: max distance in microns to move up.
+   Down: max distance in microns to move down.
+   N: number of points to take between up and down (those included).
+   log: Boolean. whether to do the range with log or linear spacing. If
    range is not simetric is may give non intented log spaced values.

XY program table
+   name: will be used for saving files.
+   Use: Boolean whether to use the position or not.
+   O: order on which XY positions are called.
+   AF: Autofocus. 0 means don't use it. Any number bigger than 0 is interpreted as correct for focus every n frames.
+   D: Drift. 0 means don't use it. Any number bigger than 0 is interpreted as correct for drift every n frames.
+   X: initial X position (may change if drift correction is ON)
+   Y: initial Y position (may change if drift correction is ON)
+   Z: initial Z position (may change if Autofocus correction is ON)

XY sorting
+   "sort" button will update the order of the XY positions as
    follow. Beginning from the first position, it will find the closest
    neighbour in an iterative way, until finished. By default new
    positions are placed at the end of the order. Result is presented on
    the XY map. Used and not used positions have different colors.

MACS 
+   TODO

Valves 
+   TODO

Pumps 
+   TODO

### Dependencies

Micro-manager needs to be available. Call to initialise micro-manager is done via src/loads/mmcload.m

Data Acquisition Toolbox:
+   Communication to the National Instrument controlling brighfield is done via daq.createSession('ni') setup in src/loads/NIBrighfieldload.m
+   DAQ (MMC) for controlling MACS valves and Mother Machine valves is set in src/loads/DAQload.m
+   Pump RS232 port needs to be connected to the PC. Port address needs to be specified in src/loads/loaddevices.m


### On code strategy

I hope that the code will be "readable" to someone familiar MATLAB UI
coding, timers, and anonymous functions. Some knowledge of micro-manager
methods is adviced.

The control of the microscope done is via micro-manager MATLAB
interface. Bright-field illumination is done via a National
Instrument. The valves for microfluidics are controlled via MATLAB DAQ
toolkit. Finally the peristaltic pump is controlled via a serial port
(R232 input of the pump).

Most of the actual work is done via timer objects. They have the
advantage of naturally implementing delays, and easy to interrupt.  For
"looping" executions with variable delays, such as MACS, valves and
pumps, the StopFcn of one timer is set to start the next one. A dummy
timer is set at the end of the sequence that keeps a count of the
iterations and decides when to stop. Multiple XY positions is rather
done via a recurring timer with a fixed frequency. To save computation
and for readability, acquisition instructions are converted into
anonymous functions, such that during acquisition images are acquired by
calling the anonymous function instead of computing the specifications
everytime.

