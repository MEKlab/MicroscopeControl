function [fig,mmc,nis] = Microscope_GUI()


    %% Useful reasorces online
    %% General intro
    %% https://micro-manager.org/wiki/Micro-Manager_Programming_Guide
    %% Useful collection of scripts. 
    %% https://micro-manager.org/wiki/Example_Beanshell_scripts
    %% On prefect focus
    %% http://micro-manager.3463995.n2.nabble.com/Nikon-TI-Perfect-Focus-control-via-MATLAB-td7582188.html
    %% FOR OMERO SEE and LOGS
    %% https://gist.github.com/bramalingam/1d36f827add8f5342068

    global mmc nis devices daqbox pumpserial
    % In case you want to access this externally from the function enviroment

    lastversion = '20170616';
    
    addpath(genpath('./src/'));
    
    gui = struct;
    gui.imageDir = './OUTPUT/';
    gui.MACSfolder = './MACSprograms/';
    gui.imagePrefix = 'IMGprefix';
    gui.imagePrefixC = 0;
    gui.imageDirC = 0;
    gui.myblack = 0.2*[1,1,1];
    gui.mywhite = 0.99*[1,1,1];
    gui.defbkgcol = [0.94,0.94,0.94];
    gui.busycolor = 'yellow';
    gui.OFFcolor  = 'red';
    gui.ONcolor   = 'green';
    gui.handleMap = struct;

    % DAQ pins
    %gui.MACS2DAQ = [1,2,3,16,4]; % flow, control, 3way chip, 3way sample, pump
    %gui.MOM2DAQ = [5,6,7,8,9,10,11,12]; % first 4 used
    
    gui.MACS2DAQ = [1,9,10,16,11]; % flow, control, 3way chip, 3way sample, pump
    gui.MOM2DAQ  = [12,13,14,15]; % first 4 used
    
    %XYadq = struct;
    date = getdate();
    
    gui.figX = 1300;
    gui.figY = 720;
    
    Xoffset  = 0.005*gui.figX;
    Yoffset  = 0.005*gui.figY;
    dw1      = 5;
    bheight1 = 40;
    bwidth1   = 91;
    
    imgaxparX = 0.40;
    imgaxparY = 1-0.005;
    % https://uk.mathworks.com/matlabcentral/answers/96816-how-do-i-change-the-default-background-color-of-all-figure-objects-created-in-matlab
    prevcolor = get(0,'defaultfigurecolor');
    set(0,'defaultfigurecolor',gui.myblack);
    fig = figure('Visible','off','MenuBar','none',...
                 'Color',gui.myblack,...
                 'Position',[200,200,gui.figX,gui.figY]);
    set(0,'defaultfigurecolor',prevcolor);
    %get(fig)
    % custom pointer? https://uk.mathworks.com/help/matlab/ref/figure-properties.html#zmw57dd0e277372
    
    % disable warning while making the gui objects
    warning('off','all'); 
    
    p1dx = imgaxparX;
    p1dy = imgaxparY;
    
    %% display stuff
    gui.tabgp(1) = uitabgroup(fig,'Position',[0.005,0.2+0.005,p1dx,p1dy*0.8]);
    gui.tabs(1) = uitab(gui.tabgp(1),'Title','Live/Snap');
    gui.tabs(2) = uitab(gui.tabgp(1),'Title','Focus Profile');
    gui.tabs(3) = uitab(gui.tabgp(1),'Title','XY Map');
    
    lastc = 0;
% $$$     % TODO: use this slider to control Z-stack visualization
% $$$     gui.controls(1) = uicontrol(gui.tabs(1),'Style','slider','String','Z',...
% $$$                                 'Min',1,'Max',100,'Value',1,'SliderStep',[0.01,0.1],...
% $$$                                 'Position',[dw1,0,gui.figX*p1dx-dw1,20]);
% $$$     lastc = 1;
    
    gui.axes(1)  = axes('Position',[0,0.035,1,0.95],'Parent',gui.tabs(1));
    img = rand(512,512)*100;
    imshow(img,[],'Parent',gui.axes(1));
    gui.handleMap.imageAxes = 1;
    
    gui.axes(2)  = axes('Position',[0.10,0.1,0.86,0.50],'Parent',gui.tabs(2),'Color','white');
    %plot([0:10],[0:10],'r','LineWidth',2);
    xlabel('Z (\mum)'); ylabel('Score');

    gui.axes(3)  = axes('Position',[0.12,0.1,0.86,0.88],'Parent',gui.tabs(3));
    set(gui.tabs(3),'ButtonDownFcn',@UpdateXYmap_Callback);
    set(gui.tabs(3),'UserData',3); % axes position
    
    fimgz = 0.3;
    gui.axes(4)  = axes('Position',[0.01,0.65,fimgz,fimgz],'Parent',gui.tabs(2));
    imshow(img,[],'Parent',gui.axes(4));
    title('Max down');
    gui.axes(5)  = axes('Position',[fimgz+0.05,0.65,fimgz,fimgz],'Parent',gui.tabs(2));
    imshow(img,[],'Parent',gui.axes(5));
    title('Focus');
    gui.axes(6)  = axes('Position',[2*fimgz+0.09,0.65,fimgz,fimgz],'Parent',gui.tabs(2));
    imshow(img,[],'Parent',gui.axes(6));
    title('Max up');
    
    lastt = 3;
    gui.tabgp(2) = uitabgroup(fig,'Position',[0.005+p1dx,0.005,1-p1dx-0.005,p1dy]);
    gui.tabs(lastt+1) = uitab(gui.tabgp(2),'Title','Microscope');
    gui.tabs(lastt+2) = uitab(gui.tabgp(2),'Title','MACS');
    gui.tabs(lastt+3) = uitab(gui.tabgp(2),'Title','Mother Machine');
    gui.handleMap.('MicroscopeTab') = lastt+1;
    gui.handleMap.('MothMachTab')   = lastt+3;
    gui.handleMap.('MACSTab')       = lastt+2;
    
    gui.panels(1) = uipanel(fig,'Title','Main','FontSize',12,...
                            'Position',[0.005,0.005,p1dx,p1dy*0.2]);
    lastp = 1;
    
    pds = [1-0.005,0.005;...%% not used
           1-0.005,(1*bheight1+4*dw1)/gui.figY;...%% for saving
           1-0.005,(1*bheight1+4*dw1)/gui.figY;...%% for focus settings
           1-0.005,(1*bheight1+4*dw1)/gui.figY;...%% for drift
           1-0.005,(1*bheight1+4*dw1)/gui.figY;...%% for camera
           1-0.005,(5*bheight1+4*dw1)/gui.figY];%% for multiple channel adq
    
    titlestrs = {'','Saving','Focus','Drift','Camera','Aqduisition'};
    for i = 1:6
        gui.panels(lastp+i) = uipanel(gui.tabs(gui.handleMap.('MicroscopeTab')),'Title',titlestrs{i},'FontSize',12,...
                                      'Position',[0.005,sum(pds(1:i-1,2))+0.005,pds(i,1),pds(i,2)]);
    end
    lastp = lastp+6;
    
    %% for Z stacks
    pdzX = 0.33-0.005;
    pdzY = 1-sum(pds(:,2))-0.005;
    gui.panels(lastp+1) = uipanel(gui.tabs(gui.handleMap.('MicroscopeTab')),'Title','Z-stacks','FontSize',12,...
                                  'Position',[0.005,sum(pds(:,2))+0.005,pdzX,pdzY]);
    %% for XY stage
    pdzXYX = 0.67-0.005;
    pdzXYY = 1-sum(pds(:,2))-0.005;
    gui.panels(lastp+2) = uipanel(gui.tabs(gui.handleMap.('MicroscopeTab')),'Title','XY positions','FontSize',12,...
                                  'Position',[pdzX+2*0.005,sum(pds(:,2))+0.005,pdzXYX,pdzXYY]);
    lastp = lastp+2;
    
    %% Others
    gui.panels(lastp+1) = uipanel(gui.tabs(gui.handleMap.('MothMachTab')),'Title','Peristaltic Pump','FontSize',12,...
                                  'Position',[0.005,0.4+0.005,1-0.005,0.3]);
    gui.panels(lastp+2) = uipanel(gui.tabs(gui.handleMap.('MothMachTab')),'Title','Valves','FontSize',12,...
                                  'Position',[0.005,0.005,1-0.005,0.4-0.005]);
    gui.panels(lastp+3) = uipanel(gui.panels(lastp+2),'Title','Iterate','FontSize',12,...
                                  'Position',[0.005,0.005,0.84-0.005,1-0.005]);
    gui.panels(lastp+4) = uipanel(gui.panels(lastp+2),'Title','Switch','FontSize',12,...
                                  'Position',[0.84+0.005,0.005,0.16-0.005,1-0.005]);
    lastp = lastp+4;
    
    gui.panels(lastp+1) = uipanel(gui.tabs(gui.handleMap.('MACSTab')),'Title','MACS','FontSize',12,...
                                  'Position',[0.005,0.5+0.005,1-0.005,0.5-0.005]);
    gui.panels(lastp+2) = uipanel(gui.panels(lastp+1),'Title','Switch','FontSize',12,...
                                  'Position',[0.005,0.50+0.005,0.2-0.005,0.5-0.005]);
    gui.panels(lastp+3) = uipanel(gui.panels(lastp+1),'Title','Pulse','FontSize',12,...
                                  'Position',[0.005,0.005,0.2-0.005,0.5-0.005]);
    gui.panels(lastp+4) = uipanel(gui.panels(lastp+1),'Title','Iterate','FontSize',12,...
                                  'Position',[0.2+0.005,0.005,0.8-0.005,1-0.005]);
    lastp = lastp+4;
    
    
    %% Main Panel Handles
    Xwidth = (p1dx-0.005)*gui.figX;
    Ywidth = gui.figY*(0.2-0.005);
    bheight3 = Ywidth*0.25;
    bwidth3  = Xwidth*0.17;
    
    labelstrs = {'Quit','Snap','Live'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(1),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[dw1+(i-1)*(dw1+bwidth3),dw1,bwidth3,bheight3]);
    end
    set(gui.controls(1+lastc),'Callback',@Hquitbutton_Callback);
    set(gui.controls(2+lastc),'Callback',@Snap_Callback);
    set(gui.controls(3+lastc),'Callback',@Live_Callback);
    lastc = lastc+3;
    
    bheight2 = Ywidth*0.19;
    bwidth2  = Xwidth*0.12;
    labelstrs = {'+X','-X','100';'+Y','-Y','100';'+Z','-Z','10'};
    typestrs = {'pushbutton','pushbutton','edit';'pushbutton','pushbutton','edit';'pushbutton','pushbutton','edit'};
    count = 0;
    for i = [1:3]
        for j = [1:3]
            count = count+1;
            type = typestrs{i,j};
            label = labelstrs{i,j};
            gui.controls(lastc+count) = uicontrol(gui.panels(1),'Style',type,...
                                                  'String',label,...
                                                  'Position',[Xwidth-i*(dw1+bwidth2),dw1+(j-1)*(dw1+bheight2),bwidth2,bheight2]);
        end
    end
    for i = [1:2]
        set(gui.controls(lastc+i),'Callback',@DeltaX_Callback);
    end
    for i = [4:5]
        set(gui.controls(lastc+i),'Callback',@DeltaY_Callback);
    end
    for i = [7:8]
        set(gui.controls(lastc+i),'Callback',@DeltaZ_Callback);
    end
    set(gui.controls(lastc+1),'UserData',[1,lastc+3]);
    set(gui.controls(lastc+2),'UserData',[-1,lastc+3]);
    set(gui.controls(lastc+4),'UserData',[1,lastc+6]);
    set(gui.controls(lastc+5),'UserData',[-1,lastc+6]);
    set(gui.controls(lastc+7),'UserData',[1,lastc+9]);
    set(gui.controls(lastc+8),'UserData',[-1,lastc+9]);
    lastc = lastc+count;
    
    %'Welcome! may the focus be with you',... 
    gui.controls(1+lastc) = uicontrol(gui.panels(1),'Style','text',...
                                      'String','Welcome! Good luck today',...
                                      'FontSize',13,...
                                      'Position',[dw1,Ywidth*0.7,Xwidth,25]);
    gui.handleMap.Msgs = lastc+1;
    lastc = lastc+1;
    
    %% Saving panel handles
    gui.controls(lastc+1) = uicontrol(gui.panels(3),'Style','pushbutton','String',gui.imageDir,...
                                      'Callback',@Folderbut_Callback,...
                                      'Position',[dw1,dw1,(6*bwidth1+dw1),bheight1*0.75]);
    gui.controls(lastc+2) = uicontrol(gui.panels(3),'Style','edit','String',gui.imagePrefix,...
                                      'Callback',@PrefixEdit_Callback,...
                                      'Position',[2*dw1+(6*bwidth1+dw1),dw1,2*(bwidth1+dw1),bheight1*0.75]);
    lastc = lastc+2;
    
    %% AutoFocus
    labelstrs = {'Autofocus','1','300','0.05','0.2','PF-Nikon'};
    typestrs = {'pushbutton','edit','edit','edit','edit','pushbutton'};
    for i = [1:6]
        gui.controls(i+lastc) = uicontrol(gui.panels(4),'Style',typestrs{i},...
                                          'String',labelstrs{i},...
                                          'Position',[dw1+(i-1)*(dw1+bwidth1),dw1,bwidth1,bheight1*0.75]);
    end
    for i = 2:5
        pos = get(gui.controls(i+lastc),'Position');
        pos(4) = bheight1*0.5;
        set(gui.controls(i+lastc),'Position',pos);
    end
    set(gui.controls(1+lastc),'Callback',@Autofocus_Callback);
    % img panel, img axes, focus panel, focus axes, axes for sub images
    set(gui.controls(1+lastc),'UserData',[1,1,2,2,4,5,6]); 
    gui.handleMap.AFparams = lastc+[1:5];
    set(gui.controls(lastc+6),'Callback',@PerfectFocus_Callback);
    lastc = lastc+6;
    
    labelstrs = {'Z prg','nfft2','min wave d','max wave d'};
    for i = [1:4]
        gui.controls(i+lastc) = uicontrol(gui.panels(4),'Style','text','String',labelstrs{i},...
                                          'Position',[dw1+(i+0)*(dw1+bwidth1),bheight1*0.4+dw1,bwidth1,bheight1*0.45]);
    end
    lastc = lastc+4;

    %% Camera
    labelstrs = {{'EMCCD','CMOS'},'Set ROI','Revert ROI','','',''};
    typestrs = {'popup','pushbutton','pushbutton','','',''};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(6),'Style',typestrs{i},...
                                          'String',labelstrs{i},...
                                          'Position',[dw1+(i-1)*(dw1+bwidth1),dw1,bwidth1,bheight1*0.75]);
    end
    pos = get(gui.controls(1+lastc),'Position');
    pos(2) = -7;
    pos(4) = bheight1;
    set(gui.controls(1+lastc),'Position',pos);
    
    set(gui.controls(lastc+1),'Callback',@ChangeCamera_Callback);
    set(gui.controls(lastc+2),'Callback',@SelectROI_Callback);
    % img panel, img axes, focus panel, focus axes
    set(gui.controls(lastc+2),'UserData',get(gui.controls(gui.handleMap.AFparams(1)),'UserData'));
    set(gui.controls(lastc+3),'Callback',@DefaultROI_Callback);
    lastc = lastc+3;
    
    %% Multiple Channel Adq
    columnNames = {'Channel','Current','Use?','Exposure','Intensity','Gain','Z-offset','Z-stack','Show'};
    tableData = {'Brighfield',true,true,20,100,5,0,0,false;...
                 'mCherry',false,true,100,100,5,0,0,false;...
                 'GFP',false,true,80,100,5,0,0,false;...
                 'DAPI',false,false,10,100,5,0,0,false;...
                 'YFP',false,false,10,100,5,0,0,false;...
                 'CFP',false,false,10,100,5,0,0,false};
    gui.tables(1) = uitable(gui.panels(7),'Data',tableData,...
                            'ColumnName',columnNames,'ColumnEditable',true,...
                            'Position',[0,0,gui.figX*(1-p1dx)-dw1,3.5*(bheight1)]);
    gui.handleMap.ADQtable = 1;
    %gui.tables(1).ColumnFormat = {'char','logical','logical','numeric','numeric','numeric','numeric','numeric',{'1','2'},'logical'};
    
    labelstrs = {'RUN','STOP','0','1','1',{'Pump OFF','Pump ON'},{'Valve OFF','Valve ON'}};
    typestrs = {'pushbutton','pushbutton','edit','edit','edit','popup','popup'};
    for i = [1:7]
        gui.controls(i+lastc) = uicontrol(gui.panels(7),'Style',typestrs{i},...
                                          'String',labelstrs{i},...
                                          'Position',[dw1+(i-1)*(dw1+bwidth1),3.5*(bheight1)+dw1,bwidth1,bheight1*0.75]);
    end
    gui.handleMap.ADQinter   = 3+lastc; % map to interval
    gui.handleMap.ADQiter    = 4+lastc; % map to iterations
    gui.handleMap.AFcorrZpgr = 5+lastc; % correction Z program
    set(gui.controls(1+lastc),'Callback',@Run_Callback);
    %set(gui.controls(1+lastc),'UserData',);
    set(gui.controls(2+lastc),'Callback',@Stop_Callback);
    set(gui.controls(4+lastc),'Callback',@IntegerValue_Callback);
    for i = 3:5
        pos = get(gui.controls(i+lastc),'Position');
        pos(4) = bheight1*0.5;
        set(gui.controls(i+lastc),'Position',pos);
    end
    for i = 6:7
        pos = get(gui.controls(i+lastc),'Position');
        pos(2) = pos(2)-3;
        set(gui.controls(i+lastc),'Position',pos);
    end
    %gui.controls(lastc+1).Position(2) = 0;
    gui.handleMap.PumpToo  = 6+lastc; % map to run pump too
    gui.handleMap.ValvToo  = 7+lastc; % map to run pump too
    lastc = lastc+7;
    
    labelstrs = {'Interval (min)','Iterations','Z pgr for AF'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(7),'Style','text','String',labelstrs{i},...
                                          'Position',[dw1+(i+1)*(dw1+bwidth1),3.9*(bheight1)+dw1,bwidth1,bheight1*0.4]);
    end
    set(gui.controls(3+lastc),'Callback',@IntegerValue_Callback);
    lastc = lastc+3;    

    %% Z stack
    Xwidth = (1-p1dx-0.005)*gui.figX*pdzX;
    Ywidth = gui.figY*pdzY;
    columnNames = {'','Down','Up','N','log'};
    tableData = cell(7,length(columnNames));
    for i = 1:7
        tableData(i,:) = {['Z',num2str(i)],1,1,5,false};
    end
    gui.tables(2) = uitable(gui.panels(8),'Data',tableData,...
                            'ColumnName',columnNames,'ColumnEditable',true,...
                            'Position',[0,0.14*Ywidth+dw1,Xwidth*(1-0.005),0.68*Ywidth]);
    mintw = 40;
    set(gui.tables(2),'ColumnWidth',{mintw,mintw,mintw,mintw,mintw,mintw});
    set(gui.tables(2),'CellSelectionCallback',@IndSelect_Callback);
    gui.handleMap.Ztable = 2;
    
    bwidth2 = Xwidth*(0.225);
    bheight2 = Ywidth*(0.13);
    labelstrs = {'new','rmv','load','save'};
    typestrs = {'pushbutton','pushbutton','pushbutton','pushbutton'};
    for i = [1:4]
        gui.controls(i+lastc) = uicontrol(gui.panels(8),'Style',typestrs{i},'String',labelstrs{i},...
                                          'Position',[dw1+(i-1)*(dw1+bwidth2),dw1,bwidth2,bheight2]);
        set(gui.controls(i+lastc),'UserData',gui.handleMap.Ztable);
    end
    set(gui.controls(1+lastc),'Callback',@NewZrow_Callback);
    set(gui.controls(2+lastc),'Callback',@RemoveRow_Callback);
    set(gui.controls(3+lastc),'Callback',@LoadTable_Callback);
    set(gui.controls(4+lastc),'Callback',@SaveTable_Callback);
    lastc = lastc+4;
    
    %% XY stage
    Xwidth = (1-p1dx-0.005)*gui.figX*pdzXYX;
    bwidth2 = Xwidth*(0.2-0.005);
    bheight2 = 26;
    
    columnNames = {'Name','Use','O','AF','D','X','Y','Z'};
    tableData = cell(1,length(columnNames));
    for i = 1:1
        tableData(i,:) = {['XY',num2str(i)],false,i,0,0,0,0,0};
    end
    tableData{1,2} = true;
    gui.tables(3) = uitable(gui.panels(9),'Data',tableData,...
                            'ColumnName',columnNames,'ColumnEditable',true,...
                            'Position',[0,0,Xwidth*(0.8-0.01),0.85*gui.figY*pdzXYY]);
    mintw = 40;
    set(gui.tables(3),'ColumnWidth',{'auto',mintw,mintw,mintw,mintw,mintw,mintw,mintw}); % gui.figX*0.8*pdzXYX-dw1
    set(gui.tables(3),'CellSelectionCallback',@IndSelect_Callback);
    gui.handleMap.XYtable = 3;
    
    labelstrs = {'Sort','Save','Load','Remove','New','Move','Set'};
    typestrs = {'pushbutton','pushbutton','pushbutton','pushbutton','pushbutton','pushbutton','pushbutton'};
    for i = [1:7]
        gui.controls(i+lastc) = uicontrol(gui.panels(9),'Style',typestrs{i},'String',labelstrs{i},...
                                          'Position',[Xwidth-1*(dw1+bwidth2),dw1+(i-1)*(dw1+bheight2),bwidth2,bheight2]);
        set(gui.controls(i+lastc),'UserData',gui.handleMap.XYtable);
    end
    set(gui.controls(lastc+1),'Callback',@SortXYpos_Callback);
    set(gui.controls(lastc+2),'Callback',@SaveTable_Callback);
    set(gui.controls(lastc+3),'Callback',@LoadTable_Callback);
    set(gui.controls(lastc+4),'Callback',@RemoveRow_Callback);
    set(gui.controls(lastc+5),'Callback',@NewXYpos_Callback);
    set(gui.controls(lastc+6),'Callback',@Move2XYpos_Callback);
    set(gui.controls(lastc+7),'Callback',@Set2CurrentXYpos_Callback);
    lastc = lastc+7;
    %

    %% Pump Control
    Xwidth = (1-p1dx-0.005)*gui.figX;
    Ywidth = 0.3*gui.figY;
    gui.controls(1+lastc) = uicontrol(gui.panels(10),'Style','slider','String','flow %',...
                                      'Position',[dw1,Ywidth*0.55+dw1,Xwidth*0.4,Ywidth*0.3]);
    set(gui.controls(lastc+1),'Callback',@PumpSlider_Callback);
    set(gui.controls(lastc+1),'UserData',2+lastc); % position of edit
    % add call back
    
    lastc = lastc+1;
    bwidth2 = Xwidth*0.4*0.32;
    bheight2 = Ywidth*0.2;
    labelstrs = {'0','ON','OFF','DIR'};
    typestrs = {'edit','pushbutton','pushbutton','pushbutton'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(10),'Style',typestrs{i},'String',labelstrs{i},...
                                          'FontSize',15,...
                                          'Position',[dw1+(i-1)*(dw1+bwidth2),dw1+Ywidth*0.3,bwidth2,bheight2]);        
    end
    set(gui.controls(lastc+1),'UserData',lastc); % position of slider
    set(gui.controls(lastc+1),'Callback',@PumpFlowEdit_Callback);
    set(gui.controls(lastc+2),'Callback',@PumpFlowON_Callback);
    set(gui.controls(lastc+3),'Callback',@PumpFlowOFF_Callback);
    lastc = lastc+3;

    columnNames = {'Name','time','Flow %'};
    tableData = cell(3,length(columnNames));
    for i = 1:3
        tableData(i,:) = {['S',num2str(i)],0,100};
    end
    gui.tables(4) = uitable(gui.panels(10),'Data',tableData,...
                            'ColumnName',columnNames,'ColumnEditable',true,...
                            'Position',[dw1+Xwidth*0.71,dw1,Xwidth*0.27,Ywidth*0.85]);
    mintw = 50;
    set(gui.tables(4),'ColumnWidth',{mintw,mintw,mintw});
    set(gui.tables(4),'CellSelectionCallback',@IndSelect_Callback);
    gui.handleMap.Ptable = 4;
    
    bheight2 = Ywidth*0.15;
    labelstrs = {'RUN','STOP','Iterate =','0','New','Remove','Up','Down','Load','Save'};
    typestrs = {'pushbutton','pushbutton','text','edit','pushbutton',...
                'pushbutton','pushbutton','pushbutton','pushbutton','pushbutton'};
    dxs = [2,1,2,1,2,1,2,1,2,1];
    dys = [0,0,1,1,2,2,3,3,4,4];
    for i = [1:10]
        gui.controls(i+lastc) = uicontrol(gui.panels(10),'Style',typestrs{i},'String',labelstrs{i},...
                                           'Position',[Xwidth*0.71-dxs(i)*(dw1+bwidth2),dw1+dys(i)*(bheight2+dw1),bwidth2,bheight2]);
        set(gui.controls(i+lastc),'UserData',gui.handleMap.Ptable);
    end
    pos = get(gui.controls(3+lastc),'Position');
    pos(4) = bheight2*0.7;
    set(gui.controls(3+lastc),'Position',pos);
    set(gui.controls(3+lastc),'HorizontalAlignment','right');
    gui.handleMap.RunPumpProgram = lastc+1;
    set(gui.controls(lastc+1),'Callback',@RunPumpProgram_Callback);
    set(gui.controls(lastc+1),'UserData',[4+lastc,gui.handleMap.Ptable]);
    set(gui.controls(lastc+2),'Callback',@StopPumpProgram_Callback);
    set(gui.controls(lastc+4),'Callback',@IntegerValue_Callback);
    gui.handleMap.nP = lastc+4;
    set(gui.controls(lastc+5),'Callback',@NewPumpRow_Callback);
    set(gui.controls(lastc+6),'Callback',@RemoveRow_Callback);
    set(gui.controls(lastc+7),'Callback',@UpRow_Callback);
    set(gui.controls(lastc+8),'Callback',@DownRow_Callback);
    set(gui.controls(lastc+9),'Callback',@LoadTable_Callback);
    set(gui.controls(lastc+10),'Callback',@SaveTable_Callback);
    gui.controls(11+lastc) = uicontrol(gui.panels(10),'Style','text',...
                                       'String','Press RUN when ready (time is in min)',...
                                       'Position',[dw1,dw1+10,Xwidth*0.4,20]);
    gui.handleMap.PumpMsgs = 11+lastc;
    lastc = lastc+11;

    %% Valve control (Iteration)
    Xwidth = (0.84-0.005)*gui.figX*(1-p1dx-0.005);
    Ywidth = 0.4*gui.figY;
    bwidth2 = Xwidth/11;
    bheight2 = Ywidth*0.15;
    
    columnNames = {'Name','time','V1','V2','V3','V4'};
    tableData = cell(3,length(columnNames));
    for i = 1:3
        tableData(i,:) = {['S',num2str(i)],0,false,false,false,false};
    end
    gui.tables(5) = uitable(gui.panels(12),'Data',tableData,...
                            'ColumnName',columnNames,'ColumnEditable',true,...
                            'Position',[0,dw1+bheight2,Xwidth-dw1,Ywidth*0.55]);
    mintw = 45;
    set(gui.tables(5),'ColumnWidth',{mintw,2*mintw,mintw,mintw,mintw,mintw,mintw,mintw,mintw,mintw});
    set(gui.tables(5),'CellSelectionCallback',@IndSelect_Callback);
    gui.handleMap.Vtable = 5;
    
    labelstrs = {'RUN','STOP','Iterate=','0','New','Remove','Up','Down','Load','Save'};
    typestrs = {'pushbutton','pushbutton','text','edit','pushbutton',...
                'pushbutton','pushbutton','pushbutton','pushbutton','pushbutton'};
% $$$     dxs = [2,1,2,1,2,1,2,1,2,1];
% $$$     dys = [0,0,1,1,2,2,3,3,4,4];
    for i = [1:10]
        gui.controls(i+lastc) = uicontrol(gui.panels(11),'Style',typestrs{i},'String',labelstrs{i},...
                                          'Position',[dw1+(i-1)*(dw1+bwidth2),dw1,bwidth2,bheight2]);
        set(gui.controls(i+lastc),'UserData',gui.handleMap.Vtable);
    end
    pos = get(gui.controls(3+lastc),'Position');
    pos(4) = bheight2*0.7;
    set(gui.controls(3+lastc),'Position',pos);
    set(gui.controls(3+lastc),'HorizontalAlignment','right');
    gui.handleMap.RunValves_Callback = lastc+1;
    set(gui.controls(lastc+1),'Callback',@RunValves_Callback);
    set(gui.controls(lastc+1),'UserData',[4+lastc,gui.handleMap.Vtable]);
    set(gui.controls(lastc+2),'Callback',@StopValves_Callback);
    set(gui.controls(lastc+4),'Callback',@IntegerValue_Callback);
    gui.handleMap.nV = lastc+4;
    set(gui.controls(lastc+5),'Callback',@NewValveRow_Callback);
    set(gui.controls(lastc+6),'Callback',@RemoveRow_Callback);
    set(gui.controls(lastc+7),'Callback',@UpRow_Callback);
    set(gui.controls(lastc+8),'Callback',@DownRow_Callback);
    set(gui.controls(lastc+9),'Callback',@LoadTable_Callback);
    set(gui.controls(lastc+10),'Callback',@SaveTable_Callback);
    gui.controls(11+lastc) = uicontrol(gui.panels(11),'Style','text','String','Press RUN when ready (time in mins)',...
                                       'Position',[Xwidth*0.1+dw1,Ywidth*0.75,Xwidth*0.8,20]);
    gui.handleMap.ValveMsgs = 11+lastc;
    lastc = lastc+11;

    columnNames = {'V'};
    tableData = {false,false,false,false}';
    gui.tables(6) = uitable(gui.panels(13),'Data',tableData,...
                            'ColumnName',columnNames,'ColumnEditable',true,...
                            'Position',[dw1,dw1,Xwidth*0.15,Ywidth*0.75]);
    set(gui.tables(6),'ColumnWidth',{mintw});
    set(gui.tables(6),'CellEditCallback',@ValveSwitch_Callback);
    gui.handleMap.Vtable2 = 6;
    
    %% MACS
    Xwidth = (1-p1dx-0.005)*gui.figX*(0.8-0.005);
    Ywidth = (0.5-0.005)*gui.figY;
    columnNames = {'',''};
    tableData = {'Flow',false;'Control',false;'Sample?',false;'Chip?',false};
    gui.tables(7) = uitable(gui.panels(15),'Data',tableData,...
                            'ColumnName',columnNames,'ColumnEditable',true,...
                            'Position',[dw1,dw1,Xwidth*0.25,Ywidth*0.37]);
    set(gui.tables(7),'ColumnWidth',{mintw+15,mintw});
    set(gui.tables(7),'CellEditCallback',@MACSSwitch_Callback);
    gui.handleMap.Mtable2 = 7;
    
    columnNames = {'Name','time','Flow?','Control?','Sample?','Chip?','Pump','Snap?'};
    tableData = cell(3,length(columnNames));
    for i = 1:3
        tableData(i,:) = {['S',num2str(i)],0,false,false,false,false,0,false};
    end
    gui.tables(8) = uitable(gui.panels(17),'Data',tableData,...
                            'ColumnName',columnNames,'ColumnEditable',true,...
                            'Position',[Xwidth*0.27,dw1,Xwidth*0.72,Ywidth*0.655]);
    mintw = 45;
    set(gui.tables(8),'ColumnWidth',{'auto',mintw,mintw,mintw,mintw,mintw,mintw,mintw});
    set(gui.tables(8),'CellSelectionCallback',@IndSelect_Callback);
    gui.handleMap.Mtable = 8;
    
    bheight2 = Ywidth*0.1105;
    bwidth2  = Xwidth*0.105;
    labelstrs = {'Pump','0','Control','0','Flow','0'};
    typestrs = {'pushbutton','edit','pushbutton','edit','pushbutton','edit'};
    dxs = [2,1,2,1,2,1];
    dys = [0,0,1,1,2,2];
    for i = [1:6]
        gui.controls(i+lastc) = uicontrol(gui.panels(16),'Style',typestrs{i},'String',labelstrs{i},...
                                           'Position',[Xwidth*0.23-dxs(i)*(dw1+bwidth2)+dw1/2,dw1+dys(i)*(bheight2+dw1),bwidth2,bheight2]);
        set(gui.controls(i+lastc),'UserData',[lastc+i+1,7]); % synchronize with live table
    end
    set(gui.controls(lastc+1),'Callback',@MACSPulsePump_Callback);
    set(gui.controls(lastc+2),'Callback',@IntegerValue_Callback);
    set(gui.controls(lastc+3),'Callback',@MACSPulseControl_Callback);
    set(gui.controls(lastc+5),'Callback',@MACSPulseFlow_Callback);
    lastc = lastc+6;
    
    bheight2 = Ywidth*0.12;
    bwidth2  = Xwidth*0.125;
    labelstrs = {'RUN','STOP','Iterate =','0','New','Remove','Up','Down','Load','Save','',''};
    typestrs = {'pushbutton','pushbutton','text','edit','pushbutton',...
                'pushbutton','pushbutton','pushbutton','pushbutton',...
                'pushbutton','pushbutton','pushbutton'};
    dxs = [2,1,2,1,2,1,2,1,2,1,2,1];
    dys = [0,0,1,1,2,2,3,3,4,4,5,5];
    for i = [1:10]
        gui.controls(i+lastc) = uicontrol(gui.panels(17),'Style',typestrs{i},'String',labelstrs{i},...
                                          'Position',[Xwidth*0.27-dxs(i)*(dw1+bwidth2)+dw1/2,dw1+dys(i)*(bheight2+dw1),bwidth2,bheight2]);
        set(gui.controls(i+lastc),'UserData',gui.handleMap.Mtable);
    end
    pos = get(gui.controls(3+lastc),'Position');
    pos(4) = bheight2*0.7;
    set(gui.controls(3+lastc),'Position',pos);
    set(gui.controls(3+lastc),'HorizontalAlignment','right');
    set(gui.controls(lastc+1),'Callback',@RunMACS_Callback);
    set(gui.controls(lastc+1),'UserData',[4+lastc,gui.handleMap.Mtable]);
    set(gui.controls(lastc+2),'Callback',@StopMACS_Callback);
    set(gui.controls(lastc+4),'Callback',@IntegerValue_Callback);
    gui.handleMap.nM = lastc+4;
    set(gui.controls(lastc+5),'Callback',@NewMACSRow_Callback);
    set(gui.controls(lastc+6),'Callback',@RemoveRow_Callback);
    set(gui.controls(lastc+7),'Callback',@UpRow_Callback);
    set(gui.controls(lastc+8),'Callback',@DownRow_Callback);
    set(gui.controls(lastc+9),'Callback',@LoadTable_Callback);
    set(gui.controls(lastc+10),'Callback',@SaveTable_Callback);
    
    gui.controls(11+lastc) = uicontrol(gui.panels(17),'Style','popup','String',{''},...
                                       'Position',[Xwidth*0.27,Ywidth*0.655-2*dw1,Xwidth*0.72-2*dw1,bheight2]);
    set(gui.controls(lastc+11),'Callback',@MACSPopup_Callback);    
    set(gui.controls(lastc+11),'UserData',gui.handleMap.Mtable);    
    updateMACSprgfiles(gui.controls(lastc+11));    
    gui.controls(12+lastc) = uicontrol(gui.panels(17),'Style','text','String','Press RUN when ready (time in sec)',...
                                       'Position',[Xwidth*0.27,Ywidth*0.655+40,Xwidth*0.72-dw1,20]);
    gui.handleMap.MACSMsgs = 12+lastc;
    lastc = lastc+12;
    
    for p = 1:length(gui.panels)
        set(gui.panels(p),'BackgroundColor',gui.myblack);
        % orange
        set(gui.panels(p),'ForegroundColor',[1,0.5,0.25]); %gui.mywhite
    end
    for c = 1:length(gui.controls)
        if(strcmp(get(gui.controls(c),'Style'),'text'))
            set(gui.controls(c),'BackgroundColor',gui.myblack);
            set(gui.controls(c),'ForegroundColor',gui.mywhite);
        else
            set(gui.controls(c),'BackgroundColor',gui.mywhite);
            set(gui.controls(c),'ForegroundColor',gui.myblack);
        end
    end
    % ForegroundColor is not a valid property in MATLAB 2013a
    for t = 1:3
        %set(gui.tabs(t),'ForegroundColor',[0,0,0.6]);
        set(gui.tabs(t),'BackgroundColor',gui.mywhite);
    end
    for t = 4:length(gui.tabs)
        %set(gui.tabs(t),'ForegroundColor',[0.9,0.4,0]);%[0.6,0,0]
        set(gui.tabs(t),'BackgroundColor',gui.myblack);
    end
    
    warning('on','all'); % re-enable warnings
    
    %% Init
    %gui.tabgp(2).SelectedTab = gui.tabs(gui.handleMap.('MothMachTab'));
    %gui.tabgp(2).SelectedTab = gui.tabs(gui.handleMap.('MACSTab'));
    %     [mmc,nis,devices,daqbox,pumpserial] = loaddevices();
    %
    if(~isempty(mmc))
        gui.cameraDevice  = mmc.getCameraDevice();
        gui.piezoDevice   = mmc.getFocusDevice();
        gui.filterDevice  = 'TIFilterBlock1';
        
        jbox = mmc.getROI;
        gui.defaultROI = [jbox.x,jbox.y,jbox.width,jbox.height];
        gui.currROI    = gui.defaultROI;
        %devices.get(10) ?? and 11
        %gui.defaultROI = mmc.getROI();
        % TODO: turn all OFF ?
    end
    if(~isempty(nis))
        % turn bfield OFF
         setBrighfield(nis,0);
    end
    if(~isempty(pumpserial))
        % pump speed to GUI initial value
        setpumpspeed(pumpserial,0);
    end
    if(~isempty(daqbox))
        % set init daq values
        putvalue(daqbox.Line(gui.MACS2DAQ),zeros(size(gui.MACS2DAQ)));
    end

   % gui.controls, gui.tables -> normalizing these in MATLAB 2013
   % makes rendering all wrong... for now they will be in absolute values
    set([fig,gui.axes,gui.panels,gui.tabs],'Units','normalized');
    set(fig,'Name','MEK Scope GUI');
    set(fig,'Visible','on');
    movegui(fig,'center');
    
    %return;
    
    %% Output saving functions
    function PrefixEdit_Callback(Hobj,~)
        newPrefix = get(Hobj,'String');
        if(~strcmp(newPrefix,gui.imagePrefix))
            gui.imagePrefix = get(Hobj,'String');
            gui.imagePrefixC = 0;
            gui.imageDirC    = 0;
        end
    end
    function Folderbut_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        foldername = uigetdir();
        foldername = [foldername,'\'];
        if(ischar(foldername) && ~(strcmp(foldername,gui.imageDir)))
            set(Hobj,'String',foldername);
            gui.imageDir     = foldername;
            gui.imagePrefixC = 0;
            gui.imageDirC    = 0;
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    
    %% Main Control Functions
    function Hquitbutton_Callback(~,~)
        timers = timerfind();
        if(~isempty(timers))
            delete(timers);
        end
        if(~isempty(mmc))
            %mmcunload(mmc);
        end
        if(~isempty(pumpserial))
            %unloadpump(pumpserial);
        end
        close(fig);
    end

    function Snap_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        ADQtable = get(gui.tables(gui.handleMap.ADQtable),'Data');
        [row] = getCurrAdq(ADQtable);
        %% dummy snap
        if(row)
            channel   = ADQtable{row,1};
            %[filter,illumination] = Channel2MMC(channel);
            exposure  = ADQtable{row,4};
            intensity = ADQtable{row,5};
            gain      = ADQtable{row,6};
            
            Zoff      = ADQtable{row,7};
            Zstack    = ADQtable{row,8};
            currz     = getZposition(mmc); 
            Zrange    = [];
            
            adqcall = makeAdqCall(channel,...
                                  exposure,...
                                  intensity,...
                                  gain,...
                                  Zoff,...
                                  Zstack);
            img = adqcall();
            if(Zstack==0)
                imshow(img,[],'Parent',gui.axes(gui.handleMap.imageAxes));
            else
                [~,~,nstacks] = size(img);
                midp = max([1,round(nstacks/2)]);
                imshow(img(:,:,midp),[],'Parent',gui.axes(gui.handleMap.imageAxes));
            end
            % TODO: make location global
            set(gui.tabgp(1),'SelectedTab',gui.tabs(1));
            if((Zstack>0)||~(Zoff==0))
                setZposition(mmc,currz);
                %mmc.waitForDevice(gui.piezoDevice);
            end
            
            gui.imagePrefixC = gui.imagePrefixC+1;
            filename = [gui.imageDir,...
                        'IMG-',gui.imagePrefix,...
                        '_Snap',...
                        '_CHN-',channel,...
                        '_',num2str(gui.imagePrefixC,'%04.f'),...
                        '.mat'];
            % TODO: retrieve z program ?
            ctime = clock;
            save(filename,'img','Zrange','Zoff','ctime','lastversion');
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end

    function Live_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        livetimer = timerfind('Tag','LIVE');
        if(~isempty(livetimer))
            set(Hobj,'BackgroundColor',gui.defbkgcol);
            stop(livetimer);
        else
            ADQtable = get(gui.tables(gui.handleMap.ADQtable),'Data');
            [row] = getCurrAdq(ADQtable);            
            if(row)
                channel   = ADQtable{row,1};
                [filter,illumination] = Channel2MMC(channel);
                exposure  = ADQtable{row,4};
                intensity = ADQtable{row,5};
                gain      = ADQtable{row,6};
                t = timer;
                t.Tag = 'LIVE';
                t.StartDelay = 0; 
                t.Period = exposure/1000+0.03; % some minimal delay
                t.ExecutionMode = 'fixedRate';
                t.TimerFcn = @LiveAux; 
                t.StopFcn = {@LiveStop,illumination};
                
                switch illumination
                  case ''
                    preBfieldAdquisition(filter,illumination,gain,exposure);
                    setBrighfield(nis,1);
                  otherwise
                    preFluoAdquisition(filter,illumination,intensity,gain,exposure);
                    MMCSetSpectra(mmc,illumination,1);
                end
                % TODO: make location global
                set(gui.tabgp(1),'SelectedTab',gui.tabs(1));
                mmc.startContinuousSequenceAcquisition(1); 
                start(t);
            end
        end
    end

    function specs = getLOGall()
        if(isempty(mmc)||isempty(devices))
            specs = struct;
        else
            specs = struct;
            % 2081104: error msg here?
            %specs = getmmclog(mmc,devices);
        end
        specs.lastversion = lastversion;
        specs.ADQtable = get(gui.tables(gui.handleMap.ADQtable),'Data');
        specs.Ztable   = get(gui.tables(gui.handleMap.Ztable),'Data');
        specs.XYtable  = get(gui.tables(gui.handleMap.XYtable),'Data');
        specs.Ptable   = get(gui.tables(gui.handleMap.Ptable),'Data');
        specs.Vtable   = get(gui.tables(gui.handleMap.Vtable),'Data');
        specs.Mtable   = get(gui.tables(gui.handleMap.Mtable),'Data');
        specs.ADQiter  = str2num(get(gui.controls(gui.handleMap.ADQiter),'String'));
        specs.nP       = str2num(get(gui.controls(gui.handleMap.nP),'String'));
        specs.nV       = str2num(get(gui.controls(gui.handleMap.nV),'String'));
        specs.nM       = str2num(get(gui.controls(gui.handleMap.nM),'String'));
        %specs.AFuseZ   = str2num(get(gui.controls(gui.handleMap.AFuseZ),'String'));
        specs.Clock    = clock;
    end

    %% Adquisition Aux functions
    function [fncall] = makeAdqCall(channel,exposure,intensity,gain,Zoff,Zstack)
        [filter,illumination] = Channel2MMC(channel);
        if(Zstack>0)
            Ztable = get(gui.tables(gui.handleMap.Ztable),'Data');
            Zrange = makeZrange(Ztable{Zstack,2},Ztable{Zstack,3},Ztable{Zstack,4},Ztable{Zstack,5});
            switch illumination
              case ''
                fncall = @()adquireBfieldIMGs(Zoff,filter,illumination,exposure,gain,Zrange);
              otherwise
                fncall = @()adquireFluoIMGs(Zoff,filter,illumination,exposure,intensity,gain,Zrange);
            end
        else
            switch illumination
              case ''
                fncall = @()adquireBfieldIMG(Zoff,filter,illumination,exposure,gain);
              otherwise
                fncall = @()adquireFluoIMG(Zoff,filter,illumination,exposure,intensity,gain);
            end
        end
    end

    function [img] = adquireFluoIMG(Zoff,filter,illumination,exposure,intensity,gain)
    % single adquisition fluo
        if(~(Zoff==0))
            setZposition(mmc,getZposition(mmc)+Zoff);
        end
        preFluoAdquisition(filter,illumination,intensity,gain,exposure);
        MMCSetSpectra(mmc,illumination,1);% IluminationON
        img = MMCSnap(mmc);
        MMCSetSpectra(mmc,illumination,0);% IluminationONFF
    end
    function [img] = adquireBfieldIMG(Zoff,filter,illumination,exposure,gain)
    % single adquisition brighfield
        if(~(Zoff==0))
            setZposition(mmc,getZposition(mmc)+Zoff);
        end
        preBfieldAdquisition(filter,illumination,gain,exposure);
        setBrighfield(nis,1);% IluminationON
        img = MMCSnap(mmc);
        setBrighfield(nis,0);% IluminationONFF
    end
    
    function [img] = adquireFluoIMGs(Zoff,filter,illumination,exposure,intensity,gain,Zrange)
    % Z-ztack adquisition fluo
         Zref = getZposition(mmc)+Zoff;
         preFluoAdquisition(filter,illumination,intensity,gain,exposure);
         img = zeros(mmc.getImageWidth,mmc.getImageHeight,length(Zrange));
         OnOff = mmc.getProperty('TIPFSStatus','State');
         if(strcmp(OnOff,'On')) % set PF OFF
             mmc.setProperty('TIPFSStatus','State','Off');
         end
         for az1 = 1:length(Zrange)
             setZposition(mmc,Zrange(az1)+Zref);
             MMCSetSpectra(mmc,illumination,1);% IluminationON
             img(:,:,az1) = MMCSnap(mmc);
             MMCSetSpectra(mmc,illumination,0);% IluminationONFF
         end
         setZposition(mmc,Zref); % go back to reference
         if(strcmp(OnOff,'On')) % get PF back ON
             mmc.setProperty('TIPFSStatus','State','On');
             % is delay needed?
             % pause();
         end
    end
    function [img] = adquireBfieldIMGs(Zoff,filter,illumination,exposure,gain,Zrange)
    % Z-ztack adquisition brighfield
        Zref = getZposition(mmc)+Zoff;
        preBfieldAdquisition(filter,illumination,gain,exposure);
        img = zeros(mmc.getImageHeight,mmc.getImageWidth,length(Zrange));
        setBrighfield(nis,1);
        for az1 = 1:length(Zrange)
            setZposition(mmc,Zrange(az1)+Zref);
            img(:,:,az1) = MMCSnap(mmc);
        end
        setBrighfield(nis,0);
        setZposition(mmc,Zref); % go back to reference
    end
    
    function preFluoAdquisition(filter,illumination,intensity,gain,exposure)
        MMCSetGain(mmc,gain);
        MMCSetFilter(mmc,filter);
        MMCSetLevel(mmc,illumination,intensity);
        mmc.waitForDevice(gui.filterDevice);
        mmc.setExposure(exposure);
    end
    function preBfieldAdquisition(filter,~,gain,exposure)
        MMCSetGain(mmc,gain);
        MMCSetFilter(mmc,filter);
        mmc.waitForDevice(gui.filterDevice);
        mmc.setExposure(exposure);
    end
    
    function [row] = getCurrAdq(ADQtable)
        curr = getMatTableCol(ADQtable,2);
        currp = find(curr==1);
        if(~isempty(currp))
            row = currp(1);
        else
            row = 0;
            display('Select at least one Current in main adquisition table');
        end
    end
    
    function LiveAux(~,~)
        img = flipdim(rot90(reshape(typecast(mmc.getLastImage,'uint16'),[mmc.getImageWidth, mmc.getImageHeight])),1);
        imshow(img,[],'Parent',gui.axes(gui.handleMap.imageAxes));
    end
    function LiveStop(Tobj,~,illumination)
        mmc.stopSequenceAcquisition();
        switch illumination
          case ''
            setBrighfield(nis,0);% IluminationONFF
          otherwise
            MMCSetSpectra(mmc,illumination,0);
        end
        delete(Tobj);
    end
    
    %% Main experiment control
    function Run_Callback(Hobj,~)
    % Main RUN for multiple adquisition
        set(Hobj,'BackgroundColor',gui.busycolor);
        % Stop all timers
        timers = timerfind();
        if(~isempty(timers))
            delete(timers);
        end        
        
        guidisplay('Parsing adquisition program');pause(0.001);
        ADQtable   = get(gui.tables(gui.handleMap.ADQtable),'Data');
        XYtable    = get(gui.tables(gui.handleMap.XYtable),'Data');
        
        interval   = str2num(get(gui.controls(gui.handleMap.ADQinter),'String'));
        iterations = str2num(get(gui.controls(gui.handleMap.ADQiter),'String'));
        % see what channels are meant to be used
        ADQuse = getMatTableCol(ADQtable,3);
        XYuse  = getMatTableCol(XYtable,2);
        if(sum(ADQuse)==0)
            % no channel was selected
            guidisplay('No channel was selected to be used');
        elseif(sum(XYuse)==0)
            % no XY was selected
            guidisplay('No XY was selected to be used');
        else
            % add a counter on the times the folder has been used
            gui.imageDirC = gui.imageDirC+1;
            outfolder = [gui.imageDir,gui.imagePrefix,'_',num2str(gui.imageDirC,'%04.f'),'/'];
            if(exist(outfolder,'dir'))
                guidisplay('Abort! a folder with that prefix already exists');
                set(Hobj,'BackgroundColor',gui.defbkgcol);
                return;
            else
                mkdir(outfolder);
            end
            
            % Get info for this experiment
            info = askinfoGUI();
            
            % Run pump and valves too?
            runpump = get(gui.controls(gui.handleMap.PumpToo),'Value')-1;
            runvalv = get(gui.controls(gui.handleMap.ValvToo),'Value')-1;
            
            ADQtouse    = find(ADQuse==1);
            channels    = ADQtable(ADQtouse,1);
            exposures   = cell2mat(ADQtable(ADQtouse,4));
            intensities = cell2mat(ADQtable(ADQtouse,5));
            gains       = cell2mat(ADQtable(ADQtouse,6));
            Zoffs       = cell2mat(ADQtable(ADQtouse,7));
            Zstacks     = cell2mat(ADQtable(ADQtouse,8));
            shows       = cell2mat(ADQtable(ADQtouse,9));
            % Make a sequence of calls that would adquire each
            adqcalls = cell(1,length(ADQtouse));
            cumZoff = 0;
            for adq = 1:length(ADQtouse)
                adqcalls{adq}  = makeAdqCall(channels{adq},...
                                             exposures(adq),...
                                             intensities(adq),...
                                             gains(adq),...                
                                             Zoffs(adq)-cumZoff,... 
                                             Zstacks(adq));
                % as all Zoff movement is relative we need to correct
                % relative to the initial Z before the first adquision.
                cumZoff = cumZoff+Zoffs(adq);
            end
            
            XYtouse = find(XYuse==1);
            % sort them by order
            [~,orders] = sort(cell2mat(XYtable(XYtouse,3)),'ascend');
            % Specs for each position            
            XYadq = struct;            
            % name of XY positions
            XYadq.names  = XYtable(XYtouse(orders),1); 
            % interval for focus correction
            XYadq.Ifocus = cell2mat(XYtable(XYtouse(orders),4)); 
            % counters for focus correction
            XYadq.focusC = zeros(size(XYadq.Ifocus));
            % Z-stack asquisition call for correction (in
            % brightfield)
            AFcorrZpgr     = str2num(get(gui.controls(gui.handleMap.AFcorrZpgr),'String'));
            XYadq.AFadq    = makeAdqCall(ADQtable{1,1},ADQtable{1,4},ADQtable{1,5},ADQtable{1,6},0,AFcorrZpgr);
            XYadq.AFZrange = makeZrange2(AFcorrZpgr);
            % interval for drift correction
            XYadq.Idrift = cell2mat(XYtable(XYtouse(orders),5));
            % counters for drift correction
            XYadq.driftC = zeros(size(XYadq.Idrift));
            % X positions
            XYadq.Xposs  = cell2mat(XYtable(XYtouse(orders),6));
            % Y positions
            XYadq.Yposs  = cell2mat(XYtable(XYtouse(orders),7));
            % Z positions
            XYadq.Zposs  = cell2mat(XYtable(XYtouse(orders),8));
            % Output saving
            XYadq.outfolder = outfolder;
            XYadq.imgPrefix = gui.imagePrefix;
            XYadq.ADQnames = channels;
            % show images as they come?
            XYadq.ADQshows = shows;
            % Counter for timer calls
            XYadq.count  = 0;
            % Some shortcuts to
            XYadq.nXY    = length(XYtouse);
            XYadq.nADQ   = length(ADQtouse);
            
            for xy = 1:XYadq.nXY
                if(XYadq.Ifocus(xy))                    
%                     % compute focus
%                     XYadq.AFs{xy}      = Autofocus_Callback(gui.controls(gui.handleMap.AFparams(1)));
%                     XYadq.AFs{xy}.dmat = makedistancematrix(XYadq.AFs{xy}.nfft2);
%                     XYadq.Zposs(xy) = XYadq.AFs{xy}.currz;
%                     %XYadq.Zposs(xy) = XYadq.AFs{xy}.focusZ;
%                     % random counter
%                     XYadq.focusC(xy) = min(floor(rand()*XYadq.Ifocus(xy))+1,XYadq.Ifocus(xy));
                end
            end
            warning('off','all');
            % Make a timer object for n repeats spaced by interval
            % that goes through each position in the order specified
            % and calls each adquisition call in that order
            t = timer;
            t.Tag = 'ADQ';
            t.StartDelay = 0.01; % some minimal delay
            t.Period = max([60*interval,0.001]); % interval
            t.ExecutionMode = 'fixedDelay';
            t.TasksToExecute = iterations;
            t.TimerFcn = {@XYstepAux,adqcalls}; %
            t.StopFcn = @AdqStopFcn;           
            
            % Save info
            filename = [XYadq.outfolder,...
                        'LOG-',XYadq.imgPrefix,...
                        '_USERSUMARY','.mat'];
            save(filename,'info');
            
            % Save log before begining
            filename = [XYadq.outfolder,...
                        'LOG-',XYadq.imgPrefix,...
                        '_init','.mat'];
            LOGspec = getLOGall();
            %LOGspec = struct;
            save(filename,'LOGspec','XYadq');
            
            if(runpump)
                RunPumpProgram_Callback(gui.controls(gui.handleMap.RunPumpProgram_Callback));
                pause(1);
            end
            if(runvalv)
                RunValves_Callback(gui.controls(gui.handleMap.RunValves_Callback));
                pause(1);
            end
            
            XYadq.tstart = tic;
            set(t,'UserData',XYadq);
            start(t);
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function XYstepAux(Tobj,~,adqcalls)
        warning('off','all');
        udata = get(Tobj,'UserData');
        udata.count = udata.count+1;
        for xy = 1:udata.nXY
            setXYStage(mmc,udata.Xposs(xy),udata.Yposs(xy));
            % Adquire
            guidisplay(['Adquiring ', udata.names{xy},' (',num2str(udata.count),')']); pause(0.001);
            for adq = 1:udata.nADQ
                deltat = toc(udata.tstart);
                filename = [udata.outfolder,...
                            'IMG-',udata.imgPrefix,...
                            '_XY-',udata.names{xy},...
                            '_CHN-',udata.ADQnames{adq},...
                            '_IT-',num2str(udata.count,'%04.f'),...
                            '.mat'];
                % Execute adquision
                img = adqcalls{adq}();
                % Save image
                save(filename,'img','deltat','lastversion');
                % Display image?
                if(udata.ADQshows(adq))
                    [nx,ny,nz] = size(img);
                    if(nz==1)
                        imshow(img,[],'Parent',gui.axes(gui.handleMap.imageAxes));
                    else
                        % Do max project by default in case of Z-stack
                        mproj    = zeros(nx,ny);
                        mproj(:) = max(reshape(img(:,:,:),nx*ny,nz)');
                        imshow(mproj,[],'Parent',gui.axes(gui.handleMap.imageAxes));
                    end
                    pause(0.02);
                end
            end
        end
        set(Tobj,'UserData',udata);
        % Go to start before next round to save on re focusing time
        setXYStage(mmc,udata.Xposs(1),udata.Yposs(1));
        guidisplay(['Iteration ', num2str(udata.count),' done!']); pause(0.001);
    end
    function AdqStopFcn(Tobj,~)
    % TODO: add a log file at the end
        XYadq = get(Tobj,'UserData');
        filename = [XYadq.outfolder,...
                    'LOG-',XYadq.imgPrefix,...
                    '_end','.mat'];
        LOGspec = getLOGall();
        save(filename,'LOGspec','XYadq');
        delete(Tobj);
        guidisplay('Adquision done!');
    end
    function Stop_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        timers = timerfind();
        if(~isempty(timers))
            delete(timers);
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end

    %% Main move
    function DeltaX_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        udata = get(Hobj,'UserData'); % [sign,handle]
        delta = udata(1)*str2num(get(gui.controls(udata(2)),'String'));
        [x,y] = getXYStage(mmc);
        setXYStage(mmc,x+delta,y);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function DeltaY_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        udata = get(Hobj,'UserData'); % [sign,handle]
        delta = udata(1)*str2num(get(gui.controls(udata(2)),'String'));
        [x,y] = getXYStage(mmc);
        setXYStage(mmc,x,y+delta);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function DeltaZ_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        udata = get(Hobj,'UserData'); % [sign,handle]
        delta = udata(1)*str2num(get(gui.controls(udata(2)),'String'));
        z = getZposition(mmc);
        setZposition(mmc,z+delta);
        %mmc.waitForDevice(gui.piezoDevice);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    
    %% Camera functions
    function ChangeCamera_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        value = get(Hobj,'Value');
        % check before if value changed
        display('Change of camera is not implemented yet')
        if(value==1) % EMCCD
            
        else % CMOS
            
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    
    %% Z menu functions
    function NewZrow_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        Ztable = get(gui.tables(gui.handleMap.Ztable),'Data');
        [nrows,~] = size(Ztable);
        Ztable(nrows+1,:) = {['Z',num2str(nrows+1)],1,1,5,false};
        set(gui.tables(gui.handleMap.Ztable),'Data',Ztable);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end

    %% XY menu Functions
    function UpdateXYmap_Callback(Hobj,~)
        udata  = get(Hobj,'UserData');
        table  = get(gui.tables(gui.handleMap.XYtable),'Data');
        names  = table(:,1);
        uses   = getMatTableCol(table,2);
        orders = getMatTableCol(table,3);
        XYs    = getMatTableCol(table,6:7);
        axes(gui.axes(udata)); 
        plot(XYs(orders,1),XYs(orders,2),'--o','Color',0.3*[1,1,1],'LineWidth',2);
        xlabel('X (\mum)'); ylabel('Y (\mum)');
        dtext = 0.01;
        for nm = 1:length(names)
            xp = XYs(orders(nm),1);
            yp = XYs(orders(nm),2);
            name = names{orders(nm)};
            use = uses(orders(nm));
            if(use)
                %
                text(xp+dtext,yp+dtext,name,'color',[1,0.25,0]);
            else
                text(xp+dtext,yp+dtext,name,'color',[0.25,0,1]);%[0,0.25,1] %[0,0.4,0.1]
            end
        end
        extrad = max([max(XYs(:,1))-min(XYs(:,1)),[max(XYs(:,2))-min(XYs(:,2))]]);
        extrad = max([0.1,extrad*0.01]);
        axis([min(XYs(:,1))-2*extrad,...
              max(XYs(:,1))+2*extrad,...
              min(XYs(:,2))-2*extrad,...
              max(XYs(:,2))+2*extrad]);
    end
    function NewXYpos_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        XYtable = get(gui.tables(gui.handleMap.XYtable),'Data');
        [nrows,~] = size(XYtable);
        orders = getMatTableCol(XYtable,3);
        [x,y] = getXYStage(mmc);
        z = getZposition(mmc);
        XYtable(nrows+1,:) = {['XY',num2str(nrows+1)],true,max(orders)+1,0,0,x,y,z};
        set(gui.tables(gui.handleMap.XYtable),'Data',XYtable);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function Move2XYpos_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        table = get(gui.tables(get(Hobj,'UserData')),'Data');
        udata = get(gui.tables(get(Hobj,'UserData')),'UserData');
        row = udata(1);
        [x,y,z] = table{row,6:8};
        setXYStage(mmc,x,y);
        setZposition(mmc,z);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function Set2CurrentXYpos_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        table = get(gui.tables(get(Hobj,'UserData')),'Data');
        udata = get(gui.tables(get(Hobj,'UserData')),'UserData');
        row = udata(1);
        [x,y] = getXYStage(mmc);
        [z] = getZposition(mmc);
        table{row,6} = x;
        table{row,7} = y;
        table{row,8} = z;
        set(gui.tables(get(Hobj,'UserData')),'Data',table);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function SortXYpos_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        table = get(gui.tables(get(Hobj,'UserData')),'Data');
        XYs    = getMatTableCol(table,6:7);
        orders = getMatTableCol(table,3);
        [nrow,~] = size(XYs);
        neworder = zeros(size(orders));
        neworder(1) = 1;
        lastXY = XYs(1,:);
        for xy = 2:nrow
            left = find(neworder==0);
            leftXY = XYs(left,:);
            leftD = (leftXY(:,1)-lastXY(1)).^2+(leftXY(:,2)-lastXY(2)).^2;
            [~,leftS] = sort(leftD,'ascend');
            nextXY = left(leftS(1));
            neworder(nextXY) = xy;
            lastXY = XYs(nextXY,:);
        end
        for xy = 1:nrow
            table{xy,3}= neworder(xy);
        end
        set(gui.tables(get(Hobj,'UserData')),'Data',table);
        % TODO: make a permanent link to this tab from guihandle struc
        UpdateXYmap_Callback(gui.tabs(3)); 
        set(gui.tabgp(1),'SelectedTab',gui.tabs(3));
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end

    %% Focus Functions
    function [AFmodel] = Autofocus_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        
        udata = get(Hobj,'UserData'); % img panel, img axes, focus panel, focus axes
        AFparams = zeros(1,4);
        for pp = 1:4
            hp = gui.handleMap.AFparams(pp+1);
            AFparams(pp) = str2num(get(gui.controls(hp),'String'));
        end               
        ADQtable = get(gui.tables(gui.handleMap.ADQtable),'Data');

        currz  = getZposition(mmc); 
        
        Zstack = AFparams(1);
        Zrange = makeZrange2(Zstack);
        
        % Snap on brighfield
        % makeAdqCall(channel,exposure,intensity,gain,Zoff,Zstack)
        adqcall = makeAdqCall(ADQtable{1,1},ADQtable{1,4},ADQtable{1,5},ADQtable{1,6},0,0);
        set(gui.tabgp(1),'SelectedTab',gui.tabs(udata(1))); 
        done = 0;
        while(done==0)
            img1 = adqcall();
            % Show image
            imshow(img1,[],'Parent',gui.axes(gui.handleMap.imageAxes));
            % Ask if want to move Z a bit before proceeding. Particularly helpful if
            % you are doing log spaced Z stacks.
            values = inputdlg({'Enter 0 to apply delta Z, 1 to continue',...
                                'Move Z by delta (um)'},...
                                'Want to move Z before proceeding?',...
                                [1,50],...
                                {'1','0'});
            if(isempty(values))
                done = 1;
            else
                done = str2num(values{1});
                dz = str2num(values{2});
                if(~(dz==0))
                    setZposition(mmc,currz+dz); % move
                    currz  = currz+dz; 
                end
            end            
        end
        
        [box] = SelectROI(udata(1));
        setROIbox(box);
        
        % do Z-stack
        guidisplay('Acquiring Z stack');
        pause(0.001);
        adqcall = makeAdqCall(ADQtable{1,1},ADQtable{1,4},ADQtable{1,5},ADQtable{1,6},0,Zstack);
        img = adqcall();

        guidisplay('Calculating focus');                
        pause(0.001);
        done = 0;
        while(done==0)
            % compute focus
            [focusZ,iscores,pos,refangles,imgs,maxZs] = findfftfocus2(img,AFparams(2),AFparams(3),AFparams(4),Zrange);
            % plot results
            cla(gui.axes(udata(4)));
            axes(gui.axes(udata(4))); 
            hold on;
            scores = iscores(1,:).*iscores(2,:);
%             scores = iscores(1,:);
            fitfn  = fit(Zrange',scores','smoothingspline');
% $$$             plot(Zrange+currz,scores(1,:)./max(scores(1,:)),'-ob','LineWidth',2);
% $$$             plot(Zrange+currz,scores(2,:)./max(scores(2,:)),'-or','LineWidth',2);
            plot(Zrange+currz,scores,'or','LineWidth',2);
            plot(Zrange+currz,fitfn(Zrange),'-r','LineWidth',2);
            plot([focusZ,focusZ]+currz,[min(scores),max(scores)],'--g','LineWidth',2);
            hold off;
            ztitles = [maxZs(1)-focusZ,0,maxZs(2)-focusZ];
            for simg = 1:3
                imshow(imgs(:,:,simg),[],'Parent',gui.axes(udata(4+simg)));
                title(gui.axes(udata(4+simg)),num2str(ztitles(simg)));
            end
            % TODO: make a permanent link to this tab from guihandle struc
            set(gui.tabgp(1),'SelectedTab',gui.tabs(2));    
            
            % Ask if wanna change parameters?
            values = inputdlg({'Enter 0 if you want to try again, 1 to exit',...
                               'nfft2',...
                               'min wave d',...
                               'max wave d'},...
                              'Is focus good?',...
                              [1,50],...
                              {'1',num2str(AFparams(2)),num2str(AFparams(3)),num2str(AFparams(4))});
            
            if(isempty(values))
                done = 1;
            else
                done = str2num(values{1});
                for pp = 2:4
                    AFparams(pp) = str2num(values{pp});
                end
            end            
        end
        clear 'img','imgs';
        focusZ = focusZ+currz;
        setZposition(mmc,focusZ); % move to focus
        setROIbox(gui.currROI); % restore ROI
        for pp = 2:4 % update parameters
            hp = gui.handleMap.AFparams(pp+1);
            set(gui.controls(hp),'String',num2str(AFparams(pp)));
        end
        % output
        AFmodel = struct;
        AFmodel.box        = box;
        AFmodel.focusZ     = focusZ;
        AFmodel.currz      = currz;
        AFmodel.Zrange     = Zrange-(focusZ-currz); % values with Z
        AFmodel.scores     = scores;
        AFmodel.fitfn      = fitfn;
        AFmodel.nfft2      = AFparams(2);
        AFmodel.minwd      = AFparams(3);
        AFmodel.maxwd      = AFparams(4);
        AFmodel.usepos     = pos; 
        AFmodel.refangles  = refangles;
        % uff... done
        guidisplay('Done!');
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function SelectROI_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        % img panel, img axes, focus panel, focus axes
        udata = get(Hobj,'UserData');
        box = SelectROI(udata(1));
        gui.currROI = box;
        setROIbox(gui.currROI);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end 
    function [box] = SelectROI(axesp)
    % $$$         width  = mmc.getImageWidth();
    % $$$         height = mmc.getImageHeight();
        width  = 512;
        height = 512;
        guidisplay('Double click on rectangle when ready');
        box = floor(selRegionIteractive(axesp));
        % we need to increase by 1
        %box(1:2) = box(1:2)+1;
    end
    function DefaultROI_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        gui.currROI = gui.defaultROI;
        setROIbox(gui.currROI);
        %mmc.setROI(gui.defaultROI);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end 
    function PerfectFocus_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        SwitchPerfectFocus(mmc);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function setROIbox(box)
        mmc.setROI(box(1),box(2),box(3),box(4)); 
    end
    %% Pump Functions
    function PumpSlider_Callback(Hobj,~)
        value = get(Hobj,'Value');
        handle = get(Hobj,'UserData');
        setpumpspeed(pumpserial,value);
        set(gui.controls(handle),'String',num2str(100*value));
    end
    function PumpFlowEdit_Callback(Hobj,~)
        value = get(Hobj,'String');
        handle = get(Hobj,'UserData');
        setpumpspeed(pumpserial,str2num(value)/100);
        set(gui.controls(handle),'Value',floor(str2num(value))/100);
    end
    function PumpFlowON_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        pumpsend(pumpserial,'1H\n');
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function PumpFlowOFF_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        pumpsend(pumpserial,'1I\n');
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function RunPumpProgram_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        stopTimerTaged('PUMP');
        udata = get(Hobj,'UserData');
        ntimes = str2num(get(gui.controls(udata(1)),'String'));
        Ptable = get(gui.tables(udata(2)),'Data');
        [nrows,~] = size(Ptable);
        if(ntimes>0)
            % extract data from table
            names = Ptable(:,1);
            %times = [0;cell2mat(Ptable(:,2))];
            times = 60.*[0;cell2mat(Ptable(:,2))]; % time in mins
            flows = cell2mat(Ptable(:,3));
            pumpr = ispumpruning(pumpserial); % save whether it currently running
            pumpv = pumpspeed(pumpserial); % save current speed
            % create timers for each step
            for r = 1:nrows
                tr = timer;
                tr.Tag = 'PUMP';
                tr.StartDelay = times(r);
                tr.TimerFcn = {@PumpStepAux,names{r},flows(r)};
                timerArray(r) = tr;
            end
            % extra timer to add last step time, cycle and stop loop
            tr = timer;
            tr.Tag = 'PUMP';
            tr.StartDelay = times(r+1);
            timerArray(nrows+1) = tr;
            % current, target 
            timerArray(nrows+1).UserData = [1,ntimes]; 
            % wonder how this plays in terms of efficiency
            timerArray(nrows+1).TimerFcn = {@stopLoopfn,'PUMP',timerArray(1),@()revertpump(pumpr,pumpv)}; 
            % sequential timer calling
            for r = 1:nrows
                timerArray(r).StopFcn = @(~,~)start(timerArray(r+1));
            end            
            % Start!
            % pumpsend(pumpserial,'1H\n');
            start(timerArray(1));            
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = PumpStepAux(~,~,name,flow)
        guidisplay(['Pump step ',name]);
        setpumpspeed(pumpserial,flow);
    end
    function [] = revertpump(pumpr,pumpv)
    % decide whether to stop or not
        if(pumpr==0)
            pumpsend(pumpserial,'1I\n');
        end
        % revert to previous speed   
        setpumpspeed(pumpserial,pumpv);
    end
    function StopPumpProgram_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        stopTimerTaged('PUMP');
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function NewPumpRow_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        Ptable = get(gui.tables(gui.handleMap.Ptable),'Data');
        [nrows,~] = size(Ptable);
        Ptable(nrows+1,:) = {['S',num2str(nrows+1)],0,100};
        set(gui.tables(gui.handleMap.Ptable),'Data',Ptable);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end

    %% Valve Functions
    function RunValves_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        stopTimerTaged('VALV');
        udata = get(Hobj,'UserData');
        ntimes = str2num(get(gui.controls(udata(1)),'String'));
        Vtable = get(gui.tables(udata(2)),'Data');
        [nrows,~] = size(Vtable);
        if(ntimes>0)
            % extract data from table
            names = Vtable(:,1);
            times = [0;getMatTableCol(Vtable,2)];
            valvs = getMatTableCol(Vtable,3:6);
            % create timers for each step
            for r = 1:nrows
                tr = timer;
                tr.Tag = 'VALV';
                tr.StartDelay = 60*times(r);
                tr.TimerFcn = {@valveStepAux,names{r},valvs(r,:)};
                timerArray(r) = tr;
            end
            % extra timer to add last step time, cycle and stop loop
            tr = timer;
            tr.Tag = 'VALV';
            tr.StartDelay = 60*times(r+1);
            timerArray(nrows+1) = tr;
            % current, target 
            timerArray(nrows+1).UserData = [1,ntimes]; 
            % wonder how this plays in terms of efficiency
            timerArray(nrows+1).TimerFcn = {@stopLoopfn,'VALV',timerArray(1),@revertValves}; 
            % sequential timer calling
            for r = 1:nrows
                timerArray(r).StopFcn = @(~,~)start(timerArray(r+1));
            end            
            % Start!
            start(timerArray(1));            
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function valveStepAux(~,~,name,valv)
        % Some how save a log of when it happened
        display(['Valve Step ',name,getdate()]);
        putvalue(daqbox.Line(gui.MOM2DAQ),valv);
        guidisplay(['VALV step: ',name]);pause(0.01);
    end
    function revertValves()
        table = get(gui.tables(gui.handleMap.Vtable2),'Data');
        [nrows,~] = size(table);
        for rv8 = 1:nrows
            value = table{rv8,1};
            chanel = gui.MOM2DAQ(rv8); 
            putvalue(daqbox.Line(chanel),value);
        end
        guidisplay(['VALV done!']);pause(0.01);
    end
    function StopValves_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        stopTimerTaged('VALV');
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end    
    function ValveSwitch_Callback(Hobj,event)
        row = event.Indices(1);
        table = get(Hobj,'Data');
        value = table{row,1};
        chanel = gui.MOM2DAQ(row);
        putvalue(daqbox.Line(chanel),value);
    end
    function NewValveRow_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        Vtable = get(gui.tables(gui.handleMap.Vtable),'Data');
        [nrows,~] = size(Vtable);
        Vtable(nrows+1,:) = {['S',num2str(nrows+1)],0,false,false,false,false};
        set(gui.tables(gui.handleMap.Vtable),'Data',Vtable);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    
    %% MACS
    function MACSSwitch_Callback(Hobj,event)
        row = event.Indices(1);
        table = get(Hobj,'Data');
        value = table{row,2};
        chanel = gui.MACS2DAQ(row); 
        putvalue(daqbox.Line(chanel),value);
    end
    function MACSPulsePump_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        udata = get(Hobj,'UserData');
        ntimes = str2num(get(gui.controls(udata(1)),'String'));
        % make a timer that pumps after a minimal delay
        if(ntimes>0)
            t = timer;
            t.Tag = 'MACS';
            t.StartDelay = 0.001; % some minimal delay
            t.Period = 0.001; % some minimal delay
            t.ExecutionMode = 'fixedDelay';
            t.TasksToExecute = ntimes;
            t.TimerFcn = @pumpAuxfn;
            t.StopFcn = @(Tobj,~)delete(Tobj);
            start(t);
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function pumpAuxfn(~,~)
        putvalue(daqbox.Line(gui.MACS2DAQ(5)),1);
        pause(0.08);
        putvalue(daqbox.Line(gui.MACS2DAQ(5)),0);
        pause(0.08);
    end
    function MACSPulseControl_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        udata = get(Hobj,'UserData');
        delta = str2num(get(gui.controls(udata(1)),'String'));
        table = get(gui.tables(udata(2)),'Data');
        % change the valve value and have a timer that set it back
        % after delay
        [t] = MakeMACSpulseTimer(delta,udata(2),2);
        table{2,2} = true;
        set(gui.tables(udata(2)),'Data',table);
        putvalue(daqbox.Line(gui.MACS2DAQ(2)),1);
        start(t);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function MACSPulseFlow_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        udata = get(Hobj,'UserData');
        delta = str2num(get(gui.controls(udata(1)),'String'));
        table = get(gui.tables(udata(2)),'Data');
        % change the valve value and have a timer that set it back
        % after delay
        [t] = MakeMACSpulseTimer(delta,udata(2),1);
        table{1,2} = true;
        set(gui.tables(udata(2)),'Data',table);
        putvalue(daqbox.Line(gui.MACS2DAQ(1)),1);
        start(t);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [t] = MakeMACSpulseTimer(delta,handle,row)
        t = timer;
        t.Tag = 'MACS';
        t.StartDelay = delta;
        t.TimerFcn = @emptyfn;
        t.StopFcn = {@MACSpulseTimerAux,handle,row};
    end
    function MACSpulseTimerAux(Tobj,~,handle,row)
        putvalue(daqbox.Line(gui.MACS2DAQ(row)),0);
        table = get(gui.tables(handle),'Data');
        table{row,2} = false;
        set(gui.tables(handle),'Data',table);
        delete(Tobj);
    end
    function RunMACS_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        stopTimerTaged('MACS');
        udata = get(Hobj,'UserData');
        ntimes = str2num(get(gui.controls(udata(1)),'String'));
        Mtable = get(gui.tables(udata(2)),'Data');
        [nrows,~] = size(Mtable);
        if(ntimes>0)
            
            interval   = str2num(get(gui.controls(gui.handleMap.ADQinter),'String'));
            iterations = str2num(get(gui.controls(gui.handleMap.ADQiter),'String'));
            gui.imageDirC = gui.imageDirC+1;
            outfolder = [gui.imageDir,gui.imagePrefix,'_',num2str(gui.imageDirC,'%04.f'),'/'];
            if(exist(outfolder,'dir'))
                guidisplay('Abort! a folder with that prefix already exists');
                set(Hobj,'BackgroundColor',gui.defbkgcol);
                return;
            else
                mkdir(outfolder);
            end
            udata = struct;
            udata.count     = 0;
            udata.outfolder = outfolder;
            udata.imgPrefix = gui.imagePrefix;
            
            % extract data from table
            names = Mtable(:,1);
            times = [0;getMatTableCol(Mtable,2)];
            flows = getMatTableCol(Mtable,3);
            cntrs = getMatTableCol(Mtable,4);
            samps = getMatTableCol(Mtable,5);
            chips = getMatTableCol(Mtable,6);
            pumps = getMatTableCol(Mtable,7);
            snaps = getMatTableCol(Mtable,8);            
            
            if(sum(snaps)>0)
                % make adquision and xy movement callbacks
                % adqcalls,xycalls
                ADQtable = get(gui.tables(gui.handleMap.ADQtable),'Data');
                XYtable  = get(gui.tables(gui.handleMap.XYtable),'Data');
                ADQuse   = getMatTableCol(ADQtable,3);
                XYuse    = getMatTableCol(XYtable,2);
                
                ADQtouse    = find(ADQuse==1);
                channels    = ADQtable(ADQtouse,1);
                exposures   = cell2mat(ADQtable(ADQtouse,4));
                intensities = cell2mat(ADQtable(ADQtouse,5));
                gains       = cell2mat(ADQtable(ADQtouse,6));
                Zoffs       = zeros(size(intensities)); % not used
                Zstacks     = zeros(size(intensities)); % not used
                shows       = cell2mat(ADQtable(ADQtouse,9));
                % Make a sequence of calls that would adquire each
                adqcalls = cell(1,length(ADQtouse));
                cumZoff = 0;
                for adq = 1:length(ADQtouse)
                    adqcalls{adq}  = makeAdqCall(channels{adq},...
                        exposures(adq),...
                        intensities(adq),...
                        gains(adq),...
                        Zoffs(adq)-cumZoff,...
                        Zstacks(adq));
                    % as all Zoff movement is relative we need to correct
                    % relative to the initial Z before the first adquision.
                    cumZoff = cumZoff+Zoffs(adq);
                end
            
                XYtouse = find(XYuse==1);
                % sort them by order
                [~,orders] = sort(cell2mat(XYtable(XYtouse,3)),'ascend');
                xycalls = cell(1,length(XYtouse));
                udata.XYnames = XYtable(XYtouse(orders),1); 
                Xposs   = cell2mat(XYtable(XYtouse(orders),6));
                Yposs   = cell2mat(XYtable(XYtouse(orders),7));
                for xy = 1:length(XYtouse)
                    xycalls{xy} = @() setXYStage(mmc,Xposs(xy),Yposs(xy));
                end
                udata.XYnames  = XYtable(XYtouse(orders),1);
                udata.ADQnames = channels;
                udata.shows    = shows;
            else
                adqcalls = [];
                xycalls  = [];
            end

            % create timers for each step
            for r = 1:nrows
                timerArray(r) = MakeMACSstepTimer(names{r},times(r),...
                                                  flows(r),cntrs(r),...
                                                  samps(r),chips(r),...
                                                  pumps(r),snaps(r),...
                                                  adqcalls,xycalls,...
                                                  interval,iterations);
                set(timerArray(r),'UserData',udata); 
            end
            % extra timer to add last step time, cycle and stop loop
            timerArray(nrows+1) = MakeMACSstepTimer('END',times(nrows+1),...
                                                    flows(nrows),cntrs(nrows),...
                                                    samps(nrows),chips(nrows),...
                                                    0,0);
            % current, target 
            timerArray(nrows+1).UserData = [1,ntimes]; 
            % loop control
            timerArray(nrows+1).TimerFcn = {@stopLoopfn,'MACS',timerArray(1),@revertMACS}; 
            % sequential timer calling
            for r = 1:nrows
                timerArray(r).StopFcn = @(~,~)start(timerArray(r+1));
            end   
            
            filename = [udata.outfolder,udata.imgPrefix,'LOG-',udata.imgPrefix,'.mat'];
            LOGspec = getLOGall();
            save(filename,'LOGspec');
            
            % Start!
            start(timerArray(1));
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [t] = MakeMACSstepTimer(name,time,flow,cntr,samp,chip,pump,snap,adqcalls,xycalls,interval,iterations)
        t = timer;
        t.Tag = 'MACS';
        t.StartDelay = time;
        t.TimerFcn = @emptyfn;
        if(snap)
            t.TimerFcn = {@MACSsnap,name,adqcalls,xycalls};
            if(iterations>0)
                t.Period = max([60*interval,0.001]); % interval
                t.ExecutionMode = 'fixedDelay';
                t.TasksToExecute = iterations;
            else
            end
        else
            t.TimerFcn = {@MACSstepSet,name,flow,cntr,samp,chip,pump};
        end
    end
    function MACSsnap(Tobj,~,name,adqcalls,xycalls)
        udata = get(Tobj,'UserData');
        udata.count = udata.count+1;
        guidisplay(['MACS: step ',name,' (',num2str(udata.count),')']);
        for xy = 1:length(xycalls)
            % move to xy
            xycalls{xy}();
            for adq = 1:length(adqcalls)
                cdate = getdate();
                ctime = clock;
                filename = [udata.outfolder,...
                            'IMG-',udata.imgPrefix,...
                            '_ST-',name,...
                            '_XY-',udata.XYnames{xy},...
                            '_CHN-',udata.ADQnames{adq},...
                            '_IT-',num2str(udata.count,'%04.f'),...
                            '.mat'];
                % Execute adquision
                img = adqcalls{adq}();
                if(udata.shows(adq))
                    imshow(img,[],'Parent',gui.axes(gui.handleMap.imageAxes));
                    pause(0.03); % some minimal delay to see the images
                end
                % Save image
                save(filename,'img','cdate','lastversion','ctime');
            end
        end
        set(Tobj,'UserData',udata);
    end
    function MACSstepSet(Tobj,~,name,flow,cntr,samp,chip,pump)
        udata = get(Tobj,'UserData');
        udata.count = udata.count+1;
        guidisplay(['MACS: step ',name,' (',num2str(udata.count),')']);
        values = [flow,cntr,samp,chip,pump];
        putvalue(daqbox.Line(gui.MACS2DAQ(1:4)),values(1:4));
        for np = 1:pump
            pumpAuxfn();
        end
        set(Tobj,'UserData',udata);
    end
    function revertMACS()
    % set MACS to user control table values
        table = get(gui.tables(gui.handleMap.Mtable2),'Data');
        [nrows,~] = size(table);
        for rm8 = 1:nrows
            value = table{rm8,2};
            chanel = gui.MACS2DAQ(rm8); % TODO: fix
            putvalue(daqbox.Line(chanel),value);
        end
    end
    function StopMACS_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        stopTimerTaged('MACS');
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function MACSPopup_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        udata = get(Hobj,'UserData');
        val = get(Hobj,'Value');
        strs = get(Hobj,'String');
        filepath = [gui.MACSfolder,strs{val}];
        loadin = load(filepath);
        set(gui.tables(udata),'Data',loadin.table);
        updateMACSprgfiles(Hobj); % update files
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function NewMACSRow_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        Mtable = get(gui.tables(gui.handleMap.Mtable),'Data');
        [nrows,~] = size(Mtable); 
        Mtable(nrows+1,:) = {['S',num2str(nrows+1)],0,false,false,false,false,0,false};
        set(gui.tables(gui.handleMap.Mtable),'Data',Mtable);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function updateMACSprgfiles(Hobj)
        files = dir([gui.MACSfolder,'*.mat']);
        progfiles = cell(length(files),1);
        for asd3 = 1:length(files)
            progfiles{asd3} = files(asd3).name;
        end
        set(Hobj,'String',progfiles);    
    end
    
    %% Generic Table Operations
    function IndSelect_Callback(Hobj,event)
        set(Hobj,'UserData',event.Indices)
    end
    function RemoveRow_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        handle = get(Hobj,'UserData');
        select = get(gui.tables(handle),'UserData');
        if(~(isempty(select)))
            row = select(1);
            table = get(gui.tables(handle),'Data');
            table = [table(1:(row-1),:);table((row+1):end,:)];
            set(gui.tables(handle),'Data',table);
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function UpRow_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        handle = get(Hobj,'UserData');
        select = get(gui.tables(handle),'UserData');
        if(~(isempty(select)||(select(1)==1)))
            table = get(gui.tables(handle),'Data');
            row = select(1);
            table = [table(1:(row-2),:);...
                     table(row,:);...
                     table(row-1,:);...
                     table((row+1):end,:)];
            set(gui.tables(handle),'Data',table);
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function DownRow_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        handle = get(Hobj,'UserData');
        select = get(gui.tables(handle),'UserData');
        table = get(gui.tables(handle),'Data');
        [nrows,~] = size(table);
        if(~(isempty(select)))
            row = select(1);
            if(~(row==nrows))
                table = [table(1:(row-1),:);...
                         table(row+1,:);...
                         table(row,:);...
                         table((row+2):end,:)];
                set(gui.tables(handle),'Data',table);
            end
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function LoadTable_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        handle = get(Hobj,'UserData');
        [filename,filepath] = uigetfile(['./','.mat'],'Load table');
        if(~(filename==0))
            loadin = load([filepath,filename]);
            set(gui.tables(handle),'Data',loadin.table);
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function SaveTable_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        handle = get(Hobj,'UserData');
        [filename,filepath] = uiputfile(['./','.mat'],'Save table');
        if(~(filename==0))
            table = get(gui.tables(handle),'Data');
            save([filepath,filename],'table','date','handle','lastversion');
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [col] = getMatTableCol(table,c)
        col = cell2mat(table(:,c));
    end
    function [row] = getMatTableRow(table,r)
        row = cell2mat(table(r,:));
    end
    %% Misc timer fns
    function DefaultEndFn(Tobj,~)
        delete(Tobj);
    end
    function stopLoopfn(Tobj,~,tag,etimer,extrafn)
        udata = get(Tobj,'UserData');
        if(udata(1)==udata(2))
            stopTimerTaged(tag);
            extrafn();
            guidisplay([tag,' loop done!']);
        else
            set(Tobj,'UserData',[udata(1)+1,udata(2)]);
            start(etimer);
        end
    end
    function emptyfn(~,~)        
    end
    function stopTimerTaged(tag)
        timers = timerfind('Tag',tag);
        if(~isempty(timers))
            stop(timers);
        end
        timers = timerfind('Tag',tag);
        if(~isempty(timers))
            delete(timers); % just in case
        end
    end

    %% Misc functions
    function guidisplay(str)
        set(gui.controls(gui.handleMap.Msgs),'String',str);
    end
    function IntegerValue_Callback(Hobj,~)
        value = str2num(get(Hobj,'String'));
        set(Hobj,'String',num2str(floor(value)));
    end
    function [pos] = selRegionIteractive(axesp);
        width  = max(get(gui.axes(axesp),'XLim'));
        height = max(get(gui.axes(axesp),'YLim'));
        px = floor(min([width,height]));
        h = imrect(gui.axes(axesp),[px/4,px/4,70,70]);
        h.setPositionConstraintFcn(makeConstrainToRectFcn('imrect',get(gui.axes(axesp),'XLim'),get(gui.axes(axesp),'YLim')));
        setResizable(h,1);
        drawnow;
        pos = wait(h);
        delete(h);
    end
    function saveIMG(filename,img,extra)
        time = clock;
        save(filename,'img','time','lastversion','extra');
        %imwrite(img,filename,'tiff','Compression','none','Description',getimgdescription());
    end
    function [Zrange] = makeZrange2(Zstack)
        Ztable = get(gui.tables(gui.handleMap.Ztable),'Data');
        Zrange = makeZrange(Ztable{Zstack,2},Ztable{Zstack,3},Ztable{Zstack,4},Ztable{Zstack,5});
    end
    function [range] = makeZrange(down,up,n,logp)
    % NOTE(1): it is kind of wierd in log space if down and up are not
    % simetric
    % NOTE(2): in linear space, the current possition may not be in
    % the range if it is not simetric or n is even 
        if(logp)
            n1 = round(n*down/(up+down));
            n2 = round(n*up/(up+down));
            range1 = logspace(0,log10(down+1),n1)-1;
            range2 = logspace(0,log10(up+1),n2)-1;
            range = [-1*fliplr(range1),range2(2:end)]; %range = range-1-down;
        else
            range = linspace(-down,up,n);
        end
    end
end
