addpath(genpath('../'));e

if(~exist(mmc))
    mmc     = mmcload();
    devices = mmc.getLoadedDevices();
end
if(~exist(nis))
    nis = NIBrighfieldload();
end

% EMCCD camera, default image width (512x512)
% if exposure is not changed, by default is 10 ms

% This is to test the speed of directions while waiting for device to
% be ready

[cx,cy] = getXYStage(mmc);
[cz] = getZposition(mmc);

dxys = linspace(0,2000,50);

ts1 = zeros(1,length(dxys));
tic;
for i = 1:length(dxys)
    t1 = toc;
    setXYStage(mmc,cx+dxys(i),cy);
    t2 = toc;
    setXYStage(mmc,cx,cy);
    ts1(i) = t2-t1;
end

ts2 = zeros(1,length(dxys));
tic;
for i = 1:length(dxys)
    t1 = toc;
    setXYStage(mmc,cx,cy+dxys(i));
    t2 = toc;
    setXYStage(mmc,cx,cy);
    ts2(i) = t2-t1;
end

ts3 = zeros(1,length(dxys));
tic;
for i = 1:length(dxys)
    t1 = toc;
    dx = dxys(i)*sin(pi/4);
    dy = dxys(i)*cos(pi/4);
    setXYStage(mmc,cx+dx,cy+dy);
    t2 = toc;
    setXYStage(mmc,cx,cy);
    ts3(i) = t2-t1;
end

ts4 = zeros(1,length(dxys));
tic;
for i = 1:length(dxys)
    t1 = toc;
    %setZposition(mmc,cz+dxys(i));
    %mmc.waitForDevice(mmc.getFocusDevice());
    t2 = toc;
    %setZposition(mmc,cz+dxys(i));
    %mmc.waitForDevice(mmc.getFocusDevice());
    ts4(i) = t2-t1;
end

fig = figure();
subplot(2,2,1);
plot(dxys,ts1);
xlabel('\DeltaX (\mum)');
ylabel('time (s)');
subplot(2,2,2);
plot(dxys,ts2);
xlabel('\DeltaY (\mum)');
ylabel('time (s)');
subplot(2,2,3);
plot(dxys,ts3);
xlabel('\DeltaXY (\mum)');
ylabel('time (s)');
subplot(2,2,4);
plot(dxys,ts4);
xlabel('\DeltaZ (\mum)');
ylabel('time (s)');

fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 15 15];
saveas(fig,'moveTests1_1.png');

