% test for talking with the pump from my laptop (ubuntu)

% My version

addpath('src/loads/');
addpath('src/pumputils/');

port = '/dev/ttyUSB0';

[pumpserial,flag] = loadpump(port);

% Find out the current speed:
pumpscan(pumpserial,'1S\n')

% for these you need to specify the size, in this case 1.

% Set operating mode to 'PUMP% Flow rate'
pumpread(pumpserial,'1L\n',1); % or M? , both are documented with the
                               % same effect

% Set panel inactive
pumpsend(pumpserial,'1B\n',1);
% Set panel active
pumpsend(pumpserial,'1A\n',1);

% Revolution counter-clockwise
pumpsend(pumpserial,'1K\n');
% Revolution clockwise
pumpsend(pumpserial,'1J\n');


% Find out whether the pump is currently running:
ret = pumpread(pumpserial,'1E\n',1)
% -> ret = -
% char 45 is - (minus)
% char 43 is + (plus)
% char 42 is * (star) I got it while busy or something in bewteen

% Turn pump ON
pumpsend(pumpserial,'1H\n');
% Turn pump OFF
pumpsend(pumpserial,'1I\n');

% Set values. 

setpumpspeed(pumpserial,0.1);
pumpscan(pumpserial,'1S\n')

% test

pumpsend(pumpserial,'1H\n');

setpumpspeed(pumpserial,0.01);
pumpscan(pumpserial,'1S\n')


pumpsend(pumpserial,'1I\n');


% number of digits after decimal
% pumpscan(pumpserial,'1[\n')

unloadpump(pumpserial);