Different kinds of tests
====================

Duration of events
---------------------

### snapTest

Test durations of adquiring images different ways.

### moveTests1

Test durations of moving stage and piezo

Focus
---------------------

### focusTest1

Basically an outline of JB method: absolute gradient. I use crossStack.mat as
example, which is a stack from a cross in the chip taking by JB.

### focusTest2

First explorations using fft2, using also crossStack as
example. Realizing that there is some difference that can be exploited
in the absolute of the frequency space, and that different wavelenghts
carry more or less information.


### focusTest3

More exploration. Begining to test my stacks which I took before going
to Paris from agar pads.

Funny note: to get good results in JB crossStack I need to take the log
of the distance to the center, whereas liner distance is enough with my
images. I suspect it has to do with the noise of the images.

### focusTest4



### focusTest5

Added phase into the mixture. If you use the maximum of the absolute as
a reference, and take the different in phase relative to these values
you can distinguish bewteen both sides from focus.
The most interesting result is the product bewteen the score from the
absolute sum in intensity and the difference in phase relative to the
previous maximum. 



