addpath(genpath('../'));
addpath('~/code/matlab/circstat/');

%% This script is a serie of test while developing the code for
%% autofocus. 

% Let try to go back to JB cross for a sec, to see if I understand
% why it was giving me different results before.

load('crossStack.mat');
[~,nstacks] = size(cstack);

fftcs = {};
nfft = 128;
for i = 1:nstacks
    fftcs{i} = fft2(cstack{i},nfft,nfft);
end
[nx,ny] = size(fftcs{1});

xmat = repmat([1:nx]-nx/2,ny,1);
ymat = repmat([1:ny]'-ny/2,1,nx);
dmat = sqrt(xmat.^2+ymat.^2+0.001); 

dmax = floor(max(max(dmat)));
[nx2 ny2] = meshgrid(1:nx,1:ny);

for j = 1:dmax
    masks{j} = (nx2 - nx/2).^2 + (ny2 - ny/2).^2 <= j.^2;
    masks{j} = double(masks{j});
end

mvalues1 = zeros(dmax,nstacks);
mvalues2 = zeros(dmax,nstacks);
for i = 1:nstacks
    v = abs(fftshift(fftcs{i}));
    vt = sum(sum(v));
    v2 = angle(fftshift(fftcs{i}));
    for j = 1:dmax-1
        mvalues1(i,j) = sum(sum((masks{j+1}-masks{j}).*dmat.*(v./vt)));
    end
    for j = 1:dmax-1
        m = (masks{j+1}-masks{j});
        pos = find(m==1);
        mvalues2(i,j) = circ_mean(v2(pos),v(pos)./vt);
    end
end

pos2 = find((masks{10}-masks{3})==1);
refangle = 62;

values2 = zeros(1,nstacks);
values3 = zeros(1,nstacks);
for i = 1:nstacks
    v = abs(fftshift(fftcs{i}));
    vt = sum(sum(v));
    v2 = dmat(pos2).*v(pos2)./vt;
    values2(i) = mean(v2);
    v3 = abs(angle(fftshift(fftcs{i}))-angle(fftshift(fftcs{refangle})));
    %values3(i) = circ_mean(v3(pos2),v(pos2)./sum(v(pos2)));
    values3(i) = circ_mean(v3(pos2),v(pos2)./vt);
end

fig = figure(); 
values4 = (values2.*(values3-pi/2));
subplot(2,2,1);
imagesc(mvalues1');
title('Magnitud score');
ylabel('wave $$\sqrt{x^2+y^2}$$','Interpreter','latex','Fontsize',13);
subplot(2,2,2);
imagesc(mvalues2');
title('Phase score');

subplot(2,2,3:4);hold on;
title('Selected scores');
plot([1:nstacks],values2*1000);
plot([1:nstacks],values3);
%axis([0 100 0 3]);
xlabel('Z (a.u.)');
ylabel('Scores');

%% it doesn't quite work. I wonder if it is because of the type of
%% image adquisition (wierd) or the fact that is a cross.


