addpath(genpath('../'));

%% This script is a serie of test while developing the code for
%% autofocus. 

% I think now I should stay with images from our scope.
% TODO: redo images as the ones I have are tilted.

%[stack,nstacks] = loadTIFstack('~/phd_microscopy/image/201704_image/20170413/20170417_e248_Gluuaa-ZsEMCCD150-11_w10 Brightfield.TIF');
[stack,nstacks] = loadTIFstack('~/phd_microscopy/image/201704_image/20170413/20170417_e248_Gluuaa-ZsEMCCD-5_w10 Brightfield.TIF');

fftcs = {};
nfft = 512;
for i = 1:nstacks
    fftcs{i} = fft2(stack(:,:,i),nfft,nfft);
end
[nx,ny] = size(fftcs{1});

xmat = repmat([1:nx]-nx/2,ny,1);
ymat = repmat([1:ny]'-ny/2,1,nx);
dmat = sqrt(xmat.^2+ymat.^2+0.001); 

dmax = floor(max(max(dmat)));
[nx2 ny2] = meshgrid(1:nx,1:ny);

for j = 1:dmax
    masks{j} = (nx2 - nx/2).^2 + (ny2 - ny/2).^2 <= j.^2;
    masks{j} = double(masks{j});
end

% I am more or less happy with the results from the previous test.
% It seems if I choose the wavelengths right, I can get a pretty good
% discriminator for focus. I suspect the cutoff is dependent on the
% camera and the type of object. I need to check that with actual
% mother machine images in our scope.

% Regarless how I cut, working with absolute fft2 makes it simetric
% in Z. I would like to have some asymmetric func, ideally monotonic.
% I suspect I need to look at the angles.

values = zeros(dmax,nstacks);
for i = 1:nstacks
    v = angle(fftshift(fftcs{i}));
    vt = sum(sum(v));
    for j = 1:dmax-1
        values(i,j) = sum(sum((masks{j+1}-masks{j}).*dmat.*(v./vt)));
    end
end
figure();
imagesc(values);
colormap('gray');
% This doesn't give me any clear Z info.

values = zeros(dmax,nstacks);
for i = 1:nstacks
    v = abs(fftshift(fftcs{i}));
    vt = sum(sum(v));
    v2 = angle(fftshift(fftcs{i}));
    for j = 1:dmax-1
        %values(i,j) = wrapTo2Pi(sum(sum((masks{j+1}-masks{j}).*v2.*(v./vt))));
        values(i,j) = sum(sum((masks{j+1}-masks{j}).*v2.*(v./vt)));
    end
end
figure();
imagesc(values);
colormap('gray');
% This one appears to be in line to what I am thinking... angles on
% one side of Z go one way, and another way after focus. However the
% effect is not very strong and shifts are different for different
% wavelengths (some turn from low to high, others from high to low).
% the best effect is before 50.

figure();
imagesc(values(1:100,1:50));
colormap('gray');

% Before we move any further, let's try to reduce the number of
% wavelengths

fftcs = {};
nfft = 100;
for i = 1:nstacks
    fftcs{i} = fft2(stack(:,:,i),nfft,nfft);
end
[nx,ny] = size(fftcs{1});

xmat = repmat([1:nx]-nx/2,ny,1);
ymat = repmat([1:ny]'-ny/2,1,nx);
dmat = sqrt(xmat.^2+ymat.^2+0.001); 

dmax = floor(max(max(dmat)));
[nx2 ny2] = meshgrid(1:nx,1:ny);

mask = (nx2 - nx/2).^2 + (ny2 - ny/2).^2 <= (nfft*0.15).^2;

values2 = zeros(1,nstacks);
for i = 1:nstacks
    v = abs(fftshift(fftcs{i}));
    vt = sum(sum(v));
    values2(i) = sum(sum(mask.*dmat.*(v./vt)));
end
figure(); hold on;
plot([1:nstacks],values2./max(values2));
xlabel('Z (a.u.)');

% So... 30 is too low, 60 you can see something, and 100 is enough.
% BTW, it seems 0.15 is a good factor.

for j = 1:dmax
    masks{j} = (nx2 - nx/2).^2 + (ny2 - ny/2).^2 <= j.^2;
    masks{j} = double(masks{j});
end

values = zeros(dmax,nstacks);
for i = 1:nstacks
    v = abs(fftshift(fftcs{i}));
    vt = sum(sum(v));
    v2 = angle(fftshift(fftcs{i}));
    for j = 1:dmax-1
        %values(i,j) = wrapTo2Pi(sum(sum((masks{j+1}-masks{j}).*v2.*(v./vt))));
        values(i,j) = sum(sum((masks{j+1}-masks{j}).*v2.*(v./vt)));
    end
end
figure();
imagesc(values);
colormap('gray');

% now at 100 you can see the effect a bit more clearly (particularly
% under 12). But still, unless we want to compute this everytime or
% pick a single region.


mask = (nx2 - nx/2).^2 + (ny2 - ny/2).^2 <= (nfft*0.15).^2;

refangle = 30;
values2 = zeros(1,nstacks);
values3 = zeros(1,nstacks);
for i = 1:nstacks
    v = abs(fftshift(fftcs{i}));
    vt = sum(sum(v));
    values2(i) = sum(sum(mask.*dmat.*(v./vt)));
    v2 = angle(fftshift(fftcs{i}))-angle(fftshift(fftcs{refangle}));
% $$$     v2 = wrapTo2Pi(angle(fftshift(fftcs{i}))-angle(fftshift(fftcs{refangle})));
% $$$     values3(i) = sum(sum((mask.*v2.*v./vt)));
% $$$     v2 = abs(wrapTo2Pi(angle(fftshift(fftcs{i}))+angle(fftshift(fftcs{refangle}))));
% $$$     values3(i) = sum(sum((mask.*v2.*(v./vt))));
end
figure(); hold on;
plot([1:nstacks],values2./max(values2));
plot([1:nstacks],values3./max(values3));
xlabel('Z (a.u.)');

% This seems cool! The magnitud is telling me how close I am to the
% focus, and the angle tells me on which side I am. However I do need
% a reference for using the angles.
% BUT: I loose the effect if I wrappTo2Pi... there is something I am missing.

% I should use the functions in here
addpath('~/code/matlab/circstat/');

values = zeros(dmax,nstacks);
for i = 1:nstacks
    v = abs(fftshift(fftcs{i}));
    vt = sum(sum(v));
    v2 = angle(fftshift(fftcs{i}));
    for j = 1:dmax-1
        %values(i,j) = wrapTo2Pi(sum(sum((masks{j+1}-masks{j}).*v2.*(v./vt))));
        m = (masks{j+1}-masks{j});
        pos = find(m==1);
        values(i,j) = circ_mean(v2(pos),v(pos)./vt);
    end
end
figure();
imagesc(values);
colormap('gray');

mask = (nx2 - nx/2).^2 + (ny2 - ny/2).^2 <= (nfft*0.15).^2;
pos = find(mask==1);
pos2 = find((masks{8}-masks{4})==1);
refangle = 62;

values2 = zeros(1,nstacks);
values3 = zeros(1,nstacks);
for i = 1:nstacks
    v = abs(fftshift(fftcs{i}));
    vt = sum(sum(v));
    v2 = dmat(pos).*v(pos)./vt;
    values2(i) = mean(v2);
    v3 = abs(angle(fftshift(fftcs{i}))-angle(fftshift(fftcs{refangle})));
    %values3(i) = circ_mean(v3(pos2),v(pos2)./sum(v(pos2)));
    values3(i) = circ_mean(v3(pos2),v(pos2)./vt);
end
figure(); 
subplot(1,2,1);hold on;
plot([1:nstacks],values2*1000);
plot([1:nstacks],values3);
xlabel('Z (a.u.)');
subplot(1,2,2);hold on;
plot([1:nstacks],values2./max(values2));
plot([1:nstacks],values3./max(values3));
xlabel('Z (a.u.)');

% Now we are talking!
% It is kind of nice that my intuition worked... but it has
% very little predictive power... only tells us whether we are up or
% down.

mask = (nx2 - nx/2).^2 + (ny2 - ny/2).^2 <= (nfft*0.15).^2;
pos = find(mask==1);
pos2 = find((masks{20}-masks{4})==1);
refangle = 62;

values2 = zeros(1,nstacks);
values3 = zeros(1,nstacks);
for i = 1:nstacks
    v = abs(fftshift(fftcs{i}));
    vt = sum(sum(v));
    v2 = dmat(pos2).*v(pos2)./vt;
    values2(i) = mean(v2);
    v3 = abs(angle(fftshift(fftcs{i}))-angle(fftshift(fftcs{refangle})));
    %values3(i) = circ_mean(v3(pos2),v(pos2)./sum(v(pos2)));
    values3(i) = circ_mean(v3(pos2),v(pos2)./vt);
end
figure(); 
values4 = (values2.*(values3-pi/2));
subplot(1,2,1);hold on;
plot([1:nstacks],values2./max(values2));
plot([1:nstacks],values3./max(values3));
subplot(1,2,2);hold on;
plot([1:nstacks],values4);
xlabel('Z (a.u.)');

% This looks even better!

% Now the question is if it works with other images. For example new
% ones without the tilted Z. Also if it works with the crosses? I
% need to look again at JB images now that I understand things a bit
% better.

% So final on this round:
addpath('~/code/matlab/circstat/');

fftcs = {};
nfft = 100;
for i = 1:nstacks
    fftcs{i} = fft2(stack(:,:,i),nfft,nfft);
end
[nx,ny] = size(fftcs{1});

xmat = repmat([1:nx]-nx/2,ny,1);
ymat = repmat([1:ny]'-ny/2,1,nx);
dmat = sqrt(xmat.^2+ymat.^2+0.001); 

dmax = floor(max(max(dmat)));
[nx2 ny2] = meshgrid(1:nx,1:ny);

for j = 1:dmax
    masks{j} = (nx2 - nx/2).^2 + (ny2 - ny/2).^2 <= j.^2;
    masks{j} = double(masks{j});
end

mvalues1 = zeros(dmax,nstacks);
mvalues2 = zeros(dmax,nstacks);
for i = 1:nstacks
    v = abs(fftshift(fftcs{i}));
    vt = sum(sum(v));
    v2 = angle(fftshift(fftcs{i}));
    for j = 1:dmax-1
        mvalues1(i,j) = sum(sum((masks{j+1}-masks{j}).*dmat.*(v./vt)));
    end
    for j = 1:dmax-1
        m = (masks{j+1}-masks{j});
        pos = find(m==1);
        mvalues2(i,j) = circ_mean(v2(pos),v(pos)./vt);
    end
end

pos2 = find((masks{20}-masks{4})==1);

values2 = zeros(1,nstacks);
for i = 1:nstacks
    v = abs(fftshift(fftcs{i}));
    vt = sum(sum(v));
    v2 = dmat(pos2).*v(pos2)./vt;
    values2(i) = mean(v2);
end
[~,refangle] = max(values2);
values3 = zeros(1,nstacks);
for i = 1:nstacks
    v = abs(fftshift(fftcs{i}));
    vt = sum(sum(v));
    v3 = abs(angle(fftshift(fftcs{i}))-angle(fftshift(fftcs{refangle})));
    %values3(i) = circ_mean(v3(pos2),v(pos2)./sum(v(pos2)));
    values3(i) = circ_mean(v3(pos2),v(pos2)./vt);
end

fig = figure(); 
%values4 = (values2.*(values3-pi/2));
subplot(2,2,1);
imagesc(mvalues1');
axis([0 nstacks 0 nfft]);
title('Magnitud score');
ylabel('wave $$\sqrt{x^2+y^2}$$','Interpreter','latex','Fontsize',13);
subplot(2,2,2);
imagesc(mvalues2');
axis([0 nstacks 0 nfft]);
title('Phase score');
subplot(2,2,3:4);hold on;
title('Selected scores');
plot([1:nstacks],values2*1000);
plot([1:nstacks],values3);
axis([0 nstacks 0 max([max(values3),max(values2*1000)])]);
xlabel('Z (a.u.)');
ylabel('Scores');

% $$$ subplot(2,2,4);hold on;
% $$$ title('Exciting!');
% $$$ plot([1:nstacks],values4);
% $$$ axis([0 100 -4e-3 4e-3]);
% $$$ xlabel('Z (a.u.)');
% $$$ ylabel('Product score');

fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 15 15];
saveas(fig,'focusTest5_best2.png');