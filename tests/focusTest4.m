addpath(genpath('../'));

%% This script is a serie of test while developing the code for
%% autofocus. 

% I think now I should stay with images from our scope.
% TODO: redo images as the ones I have are tilted.

%[stack,nstacks] = loadTIFstack('~/phd_microscopy/image/201704_image/20170413/20170417_e248_Gluuaa-ZsEMCCD150-11_w10 Brightfield.TIF');
%[stack,nstacks] = loadTIFstack('~/phd_microscopy/image/201704_image/20170413/20170417_e248_Gluuaa-ZsEMCCD-5_w10 Brightfield.TIF');

fftcs = {};
nfft = 512;
for i = 1:nstacks
    fftcs{i} = fft2(stack(:,:,i),nfft,nfft);
end
[nx,ny] = size(fftcs{1});

xmat = repmat([1:nx]-nx/2,ny,1);
ymat = repmat([1:ny]'-ny/2,1,nx);
dmat = sqrt(xmat.^2+ymat.^2+0.001); 

% what we care about is
% v = abs(fftshift(fftcs{i}))
% vt = sum(sum(v));
% score = sum(sum(dmat.*(v./vt)));

figure();
pos = [40,50,60];
for i = 1:3
    mat = fftshift(fftcs{pos(i)});
    subplot(3,2,(i-1)*2+1);
    imagesc(log(abs(real(mat))+1));
    subplot(3,2,(i-1)*2+2);
    imagesc(log(abs(imag(mat))+1));
end
colormap('gray');

figure();
pos = [40,50,60];
for i = 1:3
    mat = fftshift(fftcs{pos(i)})-fftshift(fftcs{pos(i)+10});
    subplot(3,2,(i-1)*2+1);
    imagesc(log(abs(real(mat))+1));
    subplot(3,2,(i-1)*2+2);
    imagesc(log(abs(imag(mat))+1));
end
colormap('gray');

dmax = floor(max(max(dmat)));
[nx2 ny2] = meshgrid(1:nx,1:ny);

for j = 1:dmax
    masks{j} = (nx2 - nx/2).^2 + (ny2 - ny/2).^2 <= j.^2;
    masks{j} = double(masks{j});
end

figure();
imagesc(masks{100});
colormap('gray');

values = zeros(dmax,nstacks);
for i = 1:nstacks
    v = abs(fftshift(fftcs{i}));
    vt = sum(sum(v));
    for j = 1:dmax-1
        values(i,j) = sum(sum((masks{j+1}-masks{j}).*dmat.*(v./vt)));
    end
end

figure();
imagesc(values);
colormap('gray');


values2 = zeros(1,nstacks);
values3 = zeros(1,nstacks);
for i = 1:nstacks
    v = abs(fftshift(fftcs{i}));
    vt = sum(sum(v));
    values2(i) = sum(sum(masks{100}.*dmat.*(v./vt)));
    values3(i) = sum(sum((masks{200}-masks{100}).*dmat.*(v./vt)));
end
figure(); hold on;
plot([1:nstacks],values2./max(values2));
plot([1:nstacks],values3./max(values3));
xlabel('Z (a.u.)');

% this looks fairly promising. Either of them look nice... the top
% one has the advantage of being maximal in focus, whereas the bottom
% one has better range. 
% Still, I missing a value that is monotonic with Z. I suspect it is
% linked to the phase, but I don't know how.

% the values of 100 and 200 where taken from the plot above:
% figure();
% imagesc(values);
% colormap('gray');
% In principle I could automate generating the region that best for
% distinguishing with something like that.

