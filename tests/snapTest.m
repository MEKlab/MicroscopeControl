addpath(genpath('../'));

if(~exist(mmc))
    mmc     = mmcload();
    devices = mmc.getLoadedDevices();
end
if(~exist(nis))
    nis = NIBrighfieldload();
end

% EMCCD camera, default image width (512x512)
% if exposure is not changed, by default is 10 ms

% RESULTS: (DATE)
% TODO: add results

%% Single snap
tic;
mmc.snapImage;
img = mmc.getImage;
t = toc;
display(['Single snap takes ',num2str(t),' sec']);

%% Snap reshaping the image (into 2D)
tic;
mmc.snapImage;
img = flipdim(rot90(reshape(typecast(mmc.getImage,'uint16'), [mmc.getImageWidth, mmc.getImageHeight])),1);
t = toc;
display(['Single snap with image modification takes ',num2str(t),' sec']);

%% Single bfield snap
tic;
SetBrighfield(nis,1);
MMCSetFilter(mmc,'5-TRITC');
mmc.snapImage;
img = mmc.getImage;
SetBrighfield(nis,0);
t = toc;
display(['Single brighfield snap takes ',num2str(t),' sec']);

%% Single fluo snap (GFP)
tic;
MMCSetLevel(mmc,'Cyan',100);
MMCSetFilter(mmc,'3-FITC');
MMCSetSpectra(mmc,'Cyan',1);
mmc.snapImage;
img = mmc.getImage;
MMCSetSpectra(mmc,'Cyan',0);
t = toc;
display(['Single fluo snap takes ',num2str(t),' sec']);

%% ========= Now with wating for devices

%% Single snap 
tic;
mmc.snapImage;
img = mmc.getImage;
mmc.waitForDevice(mmc.getCameraDevice());
t = toc;
display(['(camera wait) Single snap takes ',num2str(t),' sec']);

%% Single bfield snap
tic;
SetBrighfield(nis,1);
MMCSetFilter(mmc,'5-TRITC');
mmc.waitForDevice(devices.get(8)); % filter
mmc.snapImage;
img = mmc.getImage;
SetBrighfield(nis,0);
t = toc;
display(['(dev-8 wait) Single brighfield snap takes ',num2str(t),' sec']);

%% Single fluo snap (GFP)
tic;
MMCSetFilter(mmc,'3-FITC');
mmc.waitForDevice(devices.get(8)); % filter
MMCSetLevel(mmc,'Cyan',100);
MMCSetSpectra(mmc,'Cyan',1);
mmc.waitForDevice(devices.get(10)); % ??
mmc.snapImage;
img = mmc.getImage;
MMCSetSpectra(mmc,'Cyan',0);
t = toc;
display(['(dev-8-10 wait) Single brighfield snap takes ',num2str(t),' sec']);

%% Single fluo snap (GFP)
tic;
MMCSetFilter(mmc,'3-FITC');
MMCSetLevel(mmc,'Cyan',100);
MMCSetSpectra(mmc,'Cyan',1);
mmc.waitForSystem();
mmc.snapImage;
img = mmc.getImage;
MMCSetSpectra(mmc,'Cyan',0);
t = toc;
display(['(System wait) Single brighfield snap takes ',num2str(t),' sec']);

