% test for talking with the pump from my laptop (ubuntu)
% Mostly based on JB code

% see what ports are available
serialInfo = instrhwinfo('serial')

port = '/dev/ttyUSB0';
pumpserial = serial(port);

fopen(pumpserial);
set(pumpserial,'Terminator',13);

% Get name of the device
fprintf(pumpserial,sprintf('1#\n')); 
ret = fscanf(pumpserial);
% -> ret = IPC 501
if pumpserial.BytesAvailable
    dump = char(fread(pumpserial,pumpserial.BytesAvailable)'); % Just to get rid ofthe last byte
end

% Find calibration
fprintf(pumpserial,sprintf('1!\n'));
ret = fscanf(pumpserial);
% -> ret = 5.65 ml/min
if pumpserial.BytesAvailable
    dump = char(fread(pumpserial,pumpserial.BytesAvailable)'); % Just to get rid ofthe last byte
end

% Find out the current speed:
fprintf(pumpserial,sprintf('1S\n'));
ret = fscanf(pumpserial);
% -> ret = 30.0
if pumpserial.BytesAvailable
    dump = char(fread(pumpserial,pumpserial.BytesAvailable)'); % Just to get rid ofthe last byte
end

% Find out whether the pump is currently running:
fprintf(pumpserial,sprintf(['1E\n']));
ret = char(fread(pumpserial,1));
% -> ret = -
% char 45 is - (minus)
% char 43 is + (plus)
% char 42 is * (star) I got it while busy or something in bewteen

value = 0.18;
str = ['1S' num2str(100*value,'%06.f') '\n']
fprintf(pumpserial,sprintf(str));
while(pumpserial.BytesAvailable==0) pause(.05); end
ret = char(fread(pumpserial,pumpserial.BytesAvailable)'); % Just to get rid of it...

% Turn pump OFF
fprintf(pumpserial,sprintf('1I\n'));

% Turn pump ON
fprintf(pumpserial,sprintf('1H\n'));
fprintf(pumpserial,sprintf(['1E\n']));
ret = fread(pumpserial,1);
