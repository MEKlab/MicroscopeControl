addpath(genpath('../'));
load('crossStack.mat');
[~,nstacks] = size(cstack);

%% This script is a serie of test while developing the code for
%% autofocus. 

%% So, after talking briefly with Alessia, I got the idea of using
%% FFT and check which frequencies are more or less represented as a
%% function of the stack. In principles, wavelengths should get
%% bigger as you move away from the focus. Perhaps the phase of those
%% particular wavelengths could also be used.

tic;
fft2(cstack{1});
t = toc;
display(['It takes ',num2str(t),' sec']);
% computing one takes 0.02 sec in my laptop

fftcs = {};
tic;
values = zeros(1,nstacks);
% nfft = 2^8;
nfft = 2^7;
for i = 1:nstacks
    fftcs{i} = fft2(cstack{i},nfft,nfft);
end
t = toc;
display(['It takes ',num2str(t),' sec']);

pos = [1,40,50,60,100];
figure();
for i = 1:5
    subplot(5,2,(i-1)*2+1);
    imagesc(cstack{pos(i)});
    subplot(5,2,(i-1)*2+2);
    %imagesc(abs(fftcs{pos(i)}));
    temp = abs(fftshift(fftcs{pos(i)}));
    imagesc(log(temp+1));
end
colormap('gray');
% they do look different, but I don't know exactly what to extract
% from them.

pos = [1,40,50,60,100];
figure();
for i = 1:5
    subplot(5,2,(i-1)*2+1);
    imagesc(cstack{pos(i)});
    subplot(5,2,(i-1)*2+2);
    imagesc(angle(fftcs{pos(i)}));
end
colormap('gray');
% This one I don't know how to interpret.

tic;
values = zeros(1,nstacks);
for i = 1:nstacks
    temp = abs(fftshift(fftcs{i}));
    values(i) = sum(sum(temp));
end
t = toc;
display(['It takes ',num2str(t),' sec']);
figure();
plot([1:nstacks],values);
xlabel('Z (a.u.)');
% Interesting. So doing the sum of the abs of fft is equivalent to
% take the gradient and average. Although it looks a bit less noisy.

tic;
values = zeros(1,nstacks);
for i = 1:nstacks
    temp = abs(fftshift(fftcs{i}));
    values(i) = sum(sum(log(temp+1)));
end
t = toc;
display(['It takes ',num2str(t),' sec']);
figure();
plot([1:nstacks],values);
xlabel('Z (a.u.)');
% So taking the sum of the log selected for the maximum value (which
% would be the best for segmentation too).

% Let's try to filter

[nx,ny] = size(fftcs{pos(1)});
values2 = [];
for j = 1:100
    rad1 = j;
    rad2 = j+2;
    [nx2 ny2] = meshgrid(1:nx,1:ny);
    mask1 = (nx2 - nx/2).^2 + (ny2 - ny/2).^2 <= rad1.^2;
    mask2 = (nx2 - nx/2).^2 + (ny2 - ny/2).^2 <= rad2.^2;
    mask = mask2-mask1;
    values = zeros(1,nstacks);
    for i = 1:nstacks
        temp = abs(fftshift(fftcs{i}));
        values(i) = mean2(temp.*mask);
    end
    values2(j,:) = values;
end
figure();
imagesc(log(values2+1));
colormap('gray');
% based on this, the best is to take rad = 7-8 but still, at best we get
% a minimum value close to the edges.  which is kind of similar to the
% gradient.  what I want is a monotonic function.... or at least a curve
% with a single inflection point.

% I think what I want is the inverse question... what is the most
% represented wavelength (removing very small values).

fftcs = {};
nfft = 200;
for i = 1:nstacks
    fftcs{i} = fft2(cstack{i},nfft,nfft);
end

[nx,ny] = size(fftcs{pos(1)});
xmat = repmat([1:nx]-nx/2,ny,1);
ymat = repmat([1:ny]'-ny/2,1,nx);
dmat = sqrt(xmat.^2+ymat.^2);
%dmat = 1./(dmat+1);
% $$$ figure();
% $$$ imagesc(dmat);
% $$$ colormap('gray');
[nx2 ny2] = meshgrid(1:nx,1:ny);
%mask = (nx2 - nx/2).^2 + (ny2 - ny/2).^2 <= 10.^2;
mask = (nx2 - nx/2).^2 + (ny2 - ny/2).^2 <= 2.^2;
mask = 1-mask;

tic;
values = zeros(1,nstacks);
for i = 1:nstacks
    temp = abs(fftshift(fftcs{i}));
    ss = sum(sum(temp))-sum(sum(temp.*(1-mask)));
    values(i) = sum(sum(dmat.*log(temp./ss).*mask));
end
t = toc;
display(['It takes ',num2str(t),' sec']);
figure();
plot([1:nstacks],values);
xlabel('Z (a.u.)');
% it is interesting... I can find the maximums by changing nfft and
% the radius for the filter, but even if it looks close, I think the
% resulting curve looks a bit too noisy to be of use and reduce the
% number of points I need to take.
