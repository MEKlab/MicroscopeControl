addpath(genpath('../'));

%% This script is a serie of test while developing the code for
%% autofocus. 

% let's try to build this idea into fns

% case example
[img,nstacks] = loadTIFstack('~/phd_microscopy/image/201704_image/20170413/20170417_e248_Gluuaa-ZsEMCCD-5_w10 Brightfield.TIF');

%tic;
nfft = 100;
[dmat] = makedistancematrix(nfft);
[pos]  = makecylindermaskpos(dmat,nfft*0.1,nfft*0.15);

ffts = myff2(img,nfft);

score1 = computeZabsScore(ffts,dmat,pos);
[~,refp] = max(score1);
score2 = computeZangScore(ffts,dmat,pos,refp);
%toc
[~,refp2] = max(score2);
focus = round((refp2+refp)/2);
% for 501 images using nfft = 100, it takes 1 sec in my computer!

figure(); hold on;
plot([1:nstacks],score1*1000);
plot([1:nstacks],score2);

figure();
subplot(2,2,1);
imagesc(img(:,:,refp));
subplot(2,2,2);
imagesc(img(:,:,focus));
subplot(2,2,3);
imagesc(img(:,:,focus));
subplot(2,2,4);
imagesc(img(:,:,refp2));
colormap('gray');
% I think I am focusing on the dirt of the agar!

% I need to test this with more kinds of images

% $$$ figure(); hold on;
% $$$ plot([1:nstacks],score1*1000);
% $$$ plot([1:nstacks],score2);
% $$$ %plot([refp2,refp2],[0,3]);
% $$$ %plot([focus,focus],[0,3]);
% $$$ % $$$ figure(); hold on;
% $$$ % $$$ plot([1:nstacks],score2.*score1*1000);

load('crossStack.mat');
[~,nstacks] = size(cstack);

[nx,ny] = size(cstack{1});
img = zeros(nx,ny,nstacks);
for i = 1:nstacks
    img(:,:,i) = cstack{i};
end
clear 'cstack';

nfft = 100;
[dmat] = makedistancematrix(nfft);

[pos]  = makecylindermaskpos(dmat,nfft*0.01,nfft*0.1);

ffts = myff2(img,nfft);

score1 = computeZabsScore(ffts,dmat,pos);
[~,refp1] = max(score1);
score2 = computeZangScore(ffts,dmat,pos,refp1);

score3 = score2.*score1;
[~,refp2] = max(score3);
p1 = min(refp1,refp2);
p2 = max(refp1,refp2);
[~,focus] = min(score1(p1:p2));
focus = focus+p1-1;

% $$$ [~,sind] = sort(abs(score3(p1:p2)-score3(refp2)/2),'ascend');
% $$$ focus = round((refp2+refp)/2);

% $$$ figure(); hold on;
% $$$ plot([1:nstacks],score3);

fig = figure(); 
subplot(2,3,1);
imagesc(img(:,:,refp1));
subplot(2,3,2);
imagesc(img(:,:,focus));
subplot(2,3,3);
imagesc(img(:,:,refp2));
colormap('gray');
subplot(2,3,[4:6]); hold on;
plot([1:nstacks],score1/(2*10^6));
plot([1:nstacks],score2);
plot([focus,focus],[0,1.5]);

fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 15 10];
saveas(fig,'focusTest7_crossAF.png');

% I think we can turn this into a function... the tricky bit is
% finding the right range of ftt2 space.

load('crossStack.mat');
[~,nstacks] = size(cstack);

[nx,ny] = size(cstack{1});
img = zeros(nx,ny,nstacks);
for i = 1:nstacks
    img(:,:,i) = cstack{i};
end
clear 'cstack';

nfft = 100;
minf = 0.01;
maxf = 0.1;

[refps,scores,imgs] = findfftfocus(img,nfft,minf,maxf);

fig = figure(); 
subplot(2,3,1);
imagesc(imgs(:,:,1));
subplot(2,3,2);
imagesc(imgs(:,:,2));
subplot(2,3,3);
imagesc(imgs(:,:,3));
colormap('gray');
subplot(2,3,[4:6]); hold on;
score3 = scores(1,:).*scores(2,:);
plot([1:nstacks],score3,'b');
plot([refps(1),refps(1)],[0,max(score3)],'g');
plot([refps(2),refps(2)],[0,max(score3)],'r');
plot([refps(3),refps(3)],[0,max(score3)],'g');
