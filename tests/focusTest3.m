addpath(genpath('../'));
load('crossStack.mat');
[~,nstacks] = size(cstack);

%% This script is a serie of test while developing the code for
%% autofocus. 

% Let's try to follow this fft path a bit more

fftcs = {};
nfft = 300;
for i = 1:nstacks
    fftcs{i} = fft2(cstack{i},nfft,nfft);
end
[nx,ny] = size(fftcs{pos(1)});

% What we really want is to have a good measure to distingush these images
pos = [1,40,50,60,100];
figure();
for i = 1:5
    subplot(5,2,(i-1)*2+1);
    imagesc(cstack{pos(i)});
    subplot(5,2,(i-1)*2+2);
    %imagesc(abs(fftcs{pos(i)}));
    temp = abs(fftshift(fftcs{pos(i)}));
    imagesc(log(temp+1));
end
colormap('gray');

[nx,ny] = size(fftcs{pos(1)});
xmat = repmat([1:nx]-nx/2,ny,1);
ymat = repmat([1:ny]'-ny/2,1,nx);
%dmat = sqrt(xmat.^2+ymat.^2);
%dmat = log(xmat.^2+ymat.^2+0.1);
dmat = log(sqrt(xmat.^2+ymat.^2+0.001)); 
%dmat = (sqrt(xmat.^2+ymat.^2+0.1));

tic;
values = zeros(1,nstacks);
for i = 1:nstacks
    temp = abs(fftshift(fftcs{i}));
    vals = temp();
    %vals = log(temp+1);
    ss   = sum(sum(vals));
    values(i) = sum(sum(dmat.*vals./ss));
end
t = toc;
display(['It takes ',num2str(t),' sec']);
figure();
plot([1:nstacks],values);
xlabel('Z (a.u.)');

% This looks very interesting!
% I wonder how scalable it is. I should try it with other stacks.
% log of the distance and absolute wavelength gives best results.

load('JBsubstack.mat');
[~,nstacks] = size(cstack);
fftcs = {};
nfft = 300;
for i = 1:nstacks
    fftcs{i} = fft2(cstack{i},nfft,nfft);
end
[nx,ny] = size(fftcs{1});

% $$$ pos = [1,40,50,60,100];
% $$$ figure();
% $$$ for i = 1:5
% $$$     subplot(5,2,(i-1)*2+1);
% $$$     imagesc(cstack{pos(i)});
% $$$     subplot(5,2,(i-1)*2+2);
% $$$     temp = abs(fftshift(fftcs{pos(i)}));
% $$$     imagesc(log(temp+1));
% $$$ end
% $$$ colormap('gray');

[nx,ny] = size(fftcs{pos(1)});
xmat = repmat([1:nx]-nx/2,ny,1);
ymat = repmat([1:ny]'-ny/2,1,nx);
dmat = log(sqrt(xmat.^2+ymat.^2+0.001)); 
%dmat = sqrt(xmat.^2+ymat.^2+0.001); 

tic;
values = zeros(1,nstacks);
for i = 1:nstacks
    temp = abs(fftshift(fftcs{i}));
    vals = temp();
    %vals = log(temp+1);
    ss   = sum(sum(vals));
    values(i) = sum(sum(dmat.*vals./ss));
end
t = toc;
display(['It takes ',num2str(t),' sec']);
figure();
plot([1:nstacks],values);
xlabel('Z (a.u.)');

% Triying with part of the mother machine from JB images... 
% look more noisy, but similar principle

load('EMCCDstack.mat');
[~,nstacks] = size(cstack);

fftcs = {};
nfft = 200;
for i = 1:nstacks
    fftcs{i} = fft2(cstack{i},nfft,nfft);
end
[nx,ny] = size(fftcs{1});

% $$$ pos = [1,40,50,60,100];
% $$$ figure();
% $$$ for i = 1:5
% $$$     subplot(5,2,(i-1)*2+1);
% $$$     imagesc(cstack{pos(i)});
% $$$     subplot(5,2,(i-1)*2+2);
% $$$     temp = abs(fftshift(fftcs{pos(i)}));
% $$$     imagesc(log(temp+1));
% $$$ end
% $$$ colormap('gray');

[nx,ny] = size(fftcs{pos(1)});
xmat = repmat([1:nx]-nx/2,ny,1);
ymat = repmat([1:ny]'-ny/2,1,nx);
dmat = sqrt(xmat.^2+ymat.^2+0.001); 
%dmat = log(sqrt(xmat.^2+ymat.^2+0.001)); 

tic;
values = zeros(1,nstacks);
for i = 1:nstacks
    temp = abs(fftshift(fftcs{i}));
    vals = temp();
    ss   = sum(sum(vals));
    values(i) = sum(sum(dmat.*vals./ss));
end
t = toc;
display(['It takes ',num2str(t),' sec']);
[~,maxi] = max(values);
display(['Best stack: ',num2str(maxi)]);
figure();
plot([1:nstacks],values);
xlabel('Z (a.u.)');

fftcs = {};
nfft = 200;
for i = 1:nstacks
    fftcs{i} = fft2(cstack{i}(1:100,1:100),nfft,nfft);
end
[nx,ny] = size(fftcs{1});

[nx,ny] = size(fftcs{pos(1)});
xmat = repmat([1:nx]-nx/2,ny,1);
ymat = repmat([1:ny]'-ny/2,1,nx);
dmat = sqrt(xmat.^2+ymat.^2+0.001); 
%dmat = log(sqrt(xmat.^2+ymat.^2+0.001)); 

tic;
values = zeros(1,nstacks);
for i = 1:nstacks
    temp = abs(fftshift(fftcs{i}));
    vals = temp();
    ss   = sum(sum(vals));
    values(i) = sum(sum(dmat.*vals./ss));
end
t = toc;
display(['It takes ',num2str(t),' sec']);
[~,maxi] = max(values);
display(['Best stack: ',num2str(maxi)]);
figure();
plot([1:nstacks],values);
xlabel('Z (a.u.)');

% in this EMCCD data sample (I took it) the order of the stack is inverted.
% but then the behaviour is different.
% here the actual distance (not the log) seems to give the most
% interesting curve. nfft of 200
% and for the look of it, the result is a global maximum (at least in
% the range explored). 
% wonder if its because of the camera, the fact it is only cells, or
% that I have not explored enough. I did +- 50 um (if I remember correctly).

% Let's try with other data

% 
% 

[stack,nstacks] = loadTIFstack('~/phd_microscopy/image/201704_image/20170413/20170417_e248_Gluuaa-ZsEMCCD150-11_w10 Brightfield.TIF');

fftcs = {};
nfft = 512;
for i = 1:nstacks
    fftcs{i} = fft2(stack(:,:,i),nfft,nfft);
end
[nx,ny] = size(fftcs{1});

[nx,ny] = size(fftcs{pos(1)});
xmat = repmat([1:nx]-nx/2,ny,1);
ymat = repmat([1:ny]'-ny/2,1,nx);
dmat = sqrt(xmat.^2+ymat.^2+0.001); 
%dmat = log(sqrt(xmat.^2+ymat.^2+0.001)); 

tic;
values = zeros(1,nstacks);
values2 = zeros(1,nstacks);
for i = 1:nstacks
    temp = abs(fftshift(fftcs{i}));
    vals = temp;
    ss   = sum(sum(vals));
    values(i) = sum(sum(dmat.*vals./ss));
% $$$     vals2 = angle(fftshift(fftcs{i}));
% $$$     values2(i) = sum(sum(vals2.*vals./ss));
end
t = toc;
display(['It takes ',num2str(t),' sec']);
[~,maxi] = max(values);
display(['Best stack: ',num2str(maxi)]);
figure();hold on;
plot([1:nstacks],values./max(values));
%plot([1:nstacks],values2./max(values2));
xlabel('Z (a.u.)');

% looks fine actually... no? Although would be great to have a second
% value that can help us discard the tails.

[stack,nstacks] = loadTIFstack('~/phd_microscopy/image/201704_image/20170413/20170417_e248_Gluuaa-ZsEMCCD-5_w10 Brightfield.TIF');

fftcs = {};
nfft = 512;
for i = 1:nstacks
    fftcs{i} = fft2(stack(:,:,i),nfft,nfft);
end
[nx,ny] = size(fftcs{1});

[nx,ny] = size(fftcs{pos(1)});
xmat = repmat([1:nx]-nx/2,ny,1);
ymat = repmat([1:ny]'-ny/2,1,nx);
dmat = sqrt(xmat.^2+ymat.^2+0.001); 
%dmat = log(sqrt(xmat.^2+ymat.^2+0.001)); 

tic;
values = zeros(1,nstacks);
values2 = zeros(1,nstacks);
for i = 1:nstacks
    temp = abs(fftshift(fftcs{i}));
    vals = temp;
    ss   = sum(sum(vals));
    values(i) = sum(sum(dmat.*vals./ss));
% $$$     vals2 = angle(fftshift(fftcs{i}));
% $$$     values2(i) = sum(sum(vals2.*vals./ss));
end
t = toc;
display(['It takes ',num2str(t),' sec']);
[~,maxi] = max(values);
display(['Best stack: ',num2str(maxi)]);
figure();hold on;
plot([1:nstacks],values./max(values));
%plot([1:nstacks],values2./max(values2));
xlabel('Z (a.u.)');



% ================== NOT USED ==================== ->

% $$$ % we can assume that the objects we are looking for have some kind of
% $$$ % simetry
% $$$ pos = [1,40,50,60,100];
% $$$ figure();
% $$$ for i = 1:5
% $$$     subplot(5,2,(i-1)*2+1);
% $$$     imagesc(cstack{pos(i)});
% $$$     subplot(5,2,(i-1)*2+2);
% $$$     %imagesc(abs(fftcs{pos(i)}));
% $$$     temp = abs(fftshift(fftcs{pos(i)}));
% $$$     imagesc(log(temp(nx/2:end,ny/2:end)+1));
% $$$ end
% $$$ colormap('gray');
% $$$ 
% $$$ [nx,ny] = size(fftcs{pos(1)});
% $$$ xmat = repmat([1:nx/2],ny/2,1);
% $$$ ymat = repmat([1:ny/2]',1,nx/2);
% $$$ %dmat = sqrt(xmat.^2+ymat.^2);
% $$$ dmat = log(xmat.^2+ymat.^2);
% $$$ 
% $$$ tic;
% $$$ values = zeros(1,nstacks);
% $$$ for i = 1:nstacks
% $$$     temp = abs(fftshift(fftcs{i}));
% $$$     vals = temp(nx/2+1:end,ny/2+1:end);
% $$$     %vals = log(temp(nx/2+1:end,ny/2+1:end)+1);
% $$$     ss   = sum(sum(vals));
% $$$     values(i) = sum(sum(dmat.*vals./ss));
% $$$ end
% $$$ t = toc;
% $$$ display(['It takes ',num2str(t),' sec']);
% $$$ figure();
% $$$ plot([1:nstacks],values);
% $$$ xlabel('Z (a.u.)');
