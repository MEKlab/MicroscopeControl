addpath(genpath('../'));
load('crossStack.mat');
[~,nstacks] = size(cstack);

%% This script is a serie of test while developing the code for
%% autofocus. 

%% Gradient
tic;
values = zeros(1,nstacks);
for i = 1:nstacks
    values(i) = mean2(imgradient(cstack{i}));
end
t = toc;
display(['It takes ',num2str(t),' sec']);
figure();
plot([1:nstacks],values);
xlabel('Z (a.u.)');
ylabel('Mean gradient');

%% Abs Gradient
tic;
values = zeros(1,nstacks);
for i = 1:nstacks
    values(i) = mean2(abs(imgradient(cstack{i})));
end
t = toc;
display(['It takes ',num2str(t),' sec']);
figure();
plot([1:nstacks],values);
xlabel('Z (a.u.)');
ylabel('Mean abs gradient');

%% Relative abs gradient
tic;
values = zeros(1,nstacks);
for i = 1:nstacks
    values(i) = mean2(abs(imgradient(cstack{i}))./cstack{i});
end
t = toc;
display(['It takes ',num2str(t),' sec']);
figure();
plot([1:nstacks],values);
xlabel('Z (a.u.)');
ylabel('Mean relative gradient');

%% So basically whether is abs or not doesn't matter much
%% And doing relative values does not increase sensitivity really.
%% In general it takes about 0.2 sec for 100 images in my laptop.

%% I wonder if there is something better we can take than the gradient.
%% It is true that we can fit the curve and interpolate focus, but
%% because the gradient is kind of simetric and what we want is a
%% local minimum, in order to actually use it I think we need at
%% least a few points to get an accurate prediction (TODO, test what
%% is the minimum number by randomly selecting stacks).
%% Would be great to have some monotonic function of Z (ideally a
%% linear function... one can dream).

tic;
values = zeros(1,nstacks);
for i = 1:nstacks
    
end
t = toc;
display(['It takes ',num2str(t),' sec']);
figure();
plot([1:nstacks],values);
xlabel('Z (a.u.)');



