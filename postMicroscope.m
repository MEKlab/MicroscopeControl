
timers = timerfind();
if(~isempty(timers))
    delete(timers);
end
if(~isempty(mmc))
    mmcunload(mmc);
end
if(~isempty(pumpserial))
    unloadpump(pumpserial);
end
if(~isempty(nis))
    delete(nis);
end
if(~isempty(daqbox))
    delete(daqbox);
end
