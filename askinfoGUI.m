function [info] = askinfoGUI()

info = struct;
myblack = 0.2*[1,1,1];
mywhite = 0.99*[1,1,1];
defbkgcol = [0.94,0.94,0.94];

dw1      = 5;
bheight1 = 40;
bwidth1   = 91;
done = 0;
fig1 = figure('Visible','off','MenuBar','none',...
              'Color',myblack,...
              'Position',[200,200,1000,500]);
lastc = 0;
controls(1) = uicontrol('Style','pushbutton',...
                            'String','Done',...
                            'Position',[500,2*dw1,bwidth1,bheight1]);
set(controls(1),'Callback',@DoneButton_Callback);
lastc = lastc+1;

controls(2) = uicontrol('Style','text',...
                        'String','Purpose',...
                        'FontSize',16,...
                        'Position',[dw1,480-(dw1+bheight1),1.5*bwidth1,bheight1]);
controls(3) = uicontrol('Style','text',...
                        'String','Strains',...
                        'FontSize',16,...
                        'Position',[dw1,480-3*(dw1+bheight1),1.5*bwidth1,bheight1]);
controls(4) = uicontrol('Style','text',...
                        'String','Condition',...
                        'FontSize',16,...
                        'Position',[dw1,480-5*(dw1+bheight1),1.5*bwidth1,bheight1]);
controls(5) = uicontrol('Style','text',...
                        'String','Comments',...
                        'FontSize',16,...
                        'Position',[dw1,480-7*(dw1+bheight1),1.5*bwidth1,bheight1]);

controls(6) = uicontrol('Style','edit',...
                        'Max',2,'Min',0,...
                        'HorizontalAlignment','left','String','',...
                        'FontSize',14,...
                        'Position',[2*dw1+1.5*bwidth1,480-2*(dw1+bheight1)+dw1,9*bwidth1,2*bheight1]);
controls(7) = uicontrol('Style','edit',...
                        'Max',2,'Min',0,...
                        'HorizontalAlignment','left','String','',...
                        'FontSize',14,...
                        'Position',[2*dw1+1.5*bwidth1,480-4*(dw1+bheight1)+dw1,9*bwidth1,2*bheight1]);
controls(8) = uicontrol('Style','edit',...
                        'Max',2,'Min',0,...
                        'HorizontalAlignment','left','String','',...
                        'FontSize',14,...
                        'Position',[2*dw1+1.5*bwidth1,480-6*(dw1+bheight1)+dw1,9*bwidth1,2*bheight1]);
controls(9) = uicontrol('Style','edit',...
                        'Max',2,'Min',0,...
                        'HorizontalAlignment','left','String','',...
                        'FontSize',14,...
                        'Position',[2*dw1+1.5*bwidth1,480-9*(dw1+bheight1)+dw1,9*bwidth1,3*bheight1]);

for c = 1:length(controls)
    if(strcmp(get(controls(c),'Style'),'text'))
        set(controls(c),'BackgroundColor',myblack);
        set(controls(c),'ForegroundColor',[1,0.5,0.25]);
    else
        set(controls(c),'BackgroundColor',mywhite);
        set(controls(c),'ForegroundColor',myblack);
    end
end

%set([fig,controls],'Units','normalized');
movegui(fig1,'center');
set(fig1,'Visible','on');
while(done==0)
    pause(0.5);
end

info.purpose   = get(controls(6),'String');
info.strains   = get(controls(7),'String');
info.condition = get(controls(8),'String');
info.comments  = get(controls(9),'String');

close(fig1);

function [] = DoneButton_Callback(~,~)
   done = 1;
end

end