function [pumpserial,flag] = loadpump(port)

sinfo = instrhwinfo('serial');

if(isempty(sinfo.SerialPorts)) % TODO: add to check if our port is present
    flag = 0;
    pumpserial = [];
else
    pumpserial = serial(port);
    fopen(pumpserial);
    set(pumpserial,'Terminator',13);
    
    ret = pumpscan(pumpserial,'1#\n') % ask for version
    %ret = pumpread(pumpserial,'1#\n',1) % ask for version

    if (char(regexp(ret,'IPC 501','match')))
        flag = 1;
    else
        unloadpump(pumpserial);
        flag = 0;
        pumpserial = [];
    end
end

end