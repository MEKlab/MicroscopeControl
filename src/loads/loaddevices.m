function [mmc,nis,devices,daqbox,pumpserial] = loaddevices()
%

[mmc,mmcflag] = mmcload();

if(mmcflag==1)
    %MMCoptions = initMMCoptions()
    devices    = mmc.getLoadedDevices();
    %Set Core properties
    mmc.setProperty('Core','Camera','Andor'); %default camera is the Andor iXon Ultra | DU897_BV | 7876 |
    %%mmc.setProperty('Core','Shutter','Spectra'); %default shutter is TIEpiShutter
    
    %For Andor
    mmc.setProperty('Andor','Binning','1'); %set binning
    mmc.setProperty('Andor','Gain','27'); %set gain
    mmc.setProperty('Andor','CCDTemperatureSetPoint','-90'); %set temp of camera chip to -90
    
% $$$     % turn OFF all ilumination
% $$$     for spec = 1:9 
% $$$         MMCSetSpectra(mmc,nis,MMCoptions,spec,0);
% $$$     end

else
    %MMCoptions = [];
    devices = [];
    display('Warning! Failed to connect to micro-manager');
end

[daqbox,~,daqflag] = DAQload();
if(~daqflag)
    display('Warning! Failed to connect to DAQ controlling valves');
end

[nis,nisflag] = NIBrighfieldload();
if(~nisflag)
    display('Warning! Failed to connect to National Instrument controlling brighfield');
end

%[pumpserial,pumpflag] = loadpump('/dev/ttyUSB0');
% [pumpserial,pumpflag] = loadpump('COM3');
% if(~pumpflag)
%     display('Warning! Failed to connect to Pump');
% end
    pumpserial= [];

end