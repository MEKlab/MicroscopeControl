function [mmc,flag] = mmcload()

% Initalize Micro-manager
    %configfile = 'C:\Micro-Manager-1.4\MMConfig_Basic_20150322.cfg';
    %configfile = 'C:\Micro-Manager-1.4\MMConfig_Basic_Nofluo_20170603.cfg';
    configfile = 'C:\Micro-Manager-1.4\MMConfig_Basic_20170608.cfg'; % different port for Spectra
    mmcfolder = 'C:\Micro-Manager-1.4\';
    flag = 0;
    if(exist(mmcfolder))
        ccd = cd;
        cd('C:\Micro-Manager-1.4\');
        import mmcorej.*;
        mmc = CMMCore;
        mmc.loadSystemConfiguration(configfile);
        display('MMCore is initialized.');
        flag = 1;
        
        %%Set Core properties
        mmc.setProperty('Core','Camera','Andor')%default camera is the Andor iXon Ultra | DU897_BV | 7876 |
        %%mmc.setProperty('Core','Shutter','Spectra') %default shutter is TIEpiShutter
        %%For Spectra
%         mmc.setProperty('Spectra','SetLE_Type','Spectra') %specify that you use Spectra Lumencor
%         %%mmc.setProperty('Spectra','White_Level','20') %Lumencor is set to 100% intensity for all chancels
        
        %%For Andor
        mmc.setProperty('Andor','Binning','1') %set binning
        mmc.setProperty('Andor','Gain','27') %set gain
        mmc.setProperty('Andor','CCDTemperatureSetPoint','-90') %set temp of camera chip to -90
        %% mmc.setProperty('Andor','EMSwitch','Off') %turns EM capabilities OFF
        cd(ccd);
    else
        mmc = [];
    end
end