function [] = unloadpump(pumpserial)

fclose(pumpserial);
delete(pumpserial);

end