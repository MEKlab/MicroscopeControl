function [] = mmcunload(mmc)

mmc.unloadAllDevices();
clear mmc;

display('MMCore is unloaded.');

end