function [relaybox,analogi,flag] = DAQload()
    
    if(~exist('daq'))
        relaybox = [];
        analogi = [];
        flag = 0;
    else
        flag = 1;
        daqreset;
        
        %Number of samples to acquire in powers of 2 (ie 64,128,256,512 etc)
        count = 256;
        %Rate at which the samples are to be acquired
        rate = 256;
        %Input voltage range
        range = 10.0;
        analogi = [];
        %analogi  = analoginput('mcc',0);
        %add channel to ai object
        %ch0 = addchannel(analogi,0);
        %ch1 = addchannel(analogi,1);
        %set the input range for the channel
        %ch0.InputRange = [-range range];
        %ch1.InputRange = [-range range];
        %set the sample rate
        %analogi.SampleRate = rate;
        %set number of samples to acquire
        %analogi.SamplesPerTrigger = count;
        %set acquisition start type
        %analogi.TriggerType = 'Immediate';
        
        relaybox = digitalio('mcc',0);%This line adds a digital I/O device (USB-1408FS) with ID=0.
        pumpset1 = addline(relaybox, 0:7,0, 'out',{'pump1';'pump2';'pump3';'pump4';'pump5';'pump6';'pump7';'pump8'});
        pumpset2 = addline(relaybox, 0:7,1, 'out',{'pump9';'pump10';'pump11';'pump12';'pump13';'pump14';'pump15';'pump16'});
        putvalue(relaybox.Line(1:16),0); %close all valves
        
    end
end