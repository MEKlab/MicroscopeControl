function [nis,flag] = NIBrighfieldload()
% Brighfield control
    if(exist('daq'))
        nis = daq.createSession('ni');
        addDigitalChannel(nis,'Dev1', 'Port1/Line0', 'OutputOnly');
        display('National Instrument controlling brighfield loaded');
        flag = 1;
    else
        nis = [];
        flag = 0;
    end
end