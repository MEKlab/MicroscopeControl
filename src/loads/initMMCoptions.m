function [MMCoptions] = initMMCoptions()

MMCoptions.Objective   = {'Apo 60x','Apo 100x','Apo TIRF','Fluor 100x'};
MMCoptions.TINosePiece = {'1-Plan Apo 60x NA 1.40 Oil','2-Plan Apo 100x NA 1.45 Oil',...
                    '3-Plan Apo TIRF 100x NA 1.45 Oil','4-Plan Fluor 100x NA 1.30 Oil'};
MMCoptions.Ilumination = {'Blue','Cyan','Green','Red','Teal','Violet','White','YG_Filter','Brighfield'};
MMCoptions.Spectra     = {'Blue_Enable','Cyan_Enable','Green_Enable','Red_Enable','Teal_Enable','Violet_Enable','White_Enable','YG_Filter'};
MMCoptions.ColourLevel = {'Blue_Level','Cyan_Level','Green_Level','Red_Level','Teal_Level','Violet_Level','White_Level'};
MMCoptions.Filter      = {'1-DAPI','2-CFPHQ','3-FITC','4-YFPHQ','5-TRITC','6-Cy5'};

end