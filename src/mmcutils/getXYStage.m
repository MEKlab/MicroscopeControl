function [x,y] = getXYStage(mmc)
    x = mmc.getXPosition('XYStage');
    y = mmc.getYPosition('XYStage');
    %mmc.setOriginXY('XYStage');
end