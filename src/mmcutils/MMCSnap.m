function img = MMCSnap(mmc)
    mmc.snapImage; %acquire a single image
    img = flipdim(rot90(reshape(typecast(mmc.getImage,'uint16'), [mmc.getImageWidth, mmc.getImageHeight])),1);
    %img = reshape(typecast(mmc.getImage,'uint16'), [mmc.getImageWidth, mmc.getImageHeight]);
end