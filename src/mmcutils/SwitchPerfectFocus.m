function [] = SwitchPerfectFocus(mmc)
    OnOff = mmc.getProperty('TIPFSStatus','State');
    if(strcmp(OnOff,'On'))
        mmc.setProperty('TIPFSStatus','State','Off');
    else
        mmc.setProperty('TIPFSStatus','State','On');
    end
    %mmc.fullFocus();
end