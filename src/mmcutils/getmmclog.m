function [specs] = getmmclog(mmc,devices)
%%
% TODO: look up getSystemState and getConfigData mmc methods

    specs = struct;
    specs.Clock = clock;
    
    for iDEV = [0:devices.size-1]
        properties = mmc.getDevicePropertyNames(devices.get(iDEV));
        devname = removestructnonchars(char(devices.get(iDEV)));
        for j = [0:properties.size-1]
            s = char(properties.get(j));
            v = mmc.getProperty(devices.get(iDEV),s);
            specs.([devname,'_',removestructnonchars(s)]) = char(v);
        end
    end

    specs.PerfectFocus = char(mmc.getProperty('TIPFSStatus','State'));

    % TODO: add this guy removestructnonchars
end