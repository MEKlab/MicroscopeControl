function [z] = getZposition(mmc)

    %z = mmc.getCurrentFocusScore;
    z = mmc.getPosition(mmc.getFocusDevice());

end