function [specsSNAP] = getsnapinfo(mmc,MMCoptions)
%%

specsSNAP.Clock = clock;
specsSNAP.MACSOnOFF = MACSOnOFF;
%% NOTE(20160225) Takes about 0.08 sec to adquire this info. 
%% If time is an issue, perhaps want to find a faster way
specsSNAP.StageXPosition = mmc.getXPosition('XYStage');
specsSNAP.StageYPosition = mmc.getYPosition('XYStage');
specsSNAP.ZPosition      = mmc.getCurrentFocusScore;
%specsSNAP.BrighFieldON   = BrighFieldON;
for it23 = 1:length(MMCoptions.Spectra)
    specsSNAP.(MMCoptions.Spectra{it23}) = char(mmc.getProperty('Spectra',MMCoptions.Spectra{it23}));
end
for it23 = 1:length(MMCoptions.ColourLevel)
    specsSNAP.(MMCoptions.ColourLevel{it23}) = char(mmc.getProperty('Spectra',MMCoptions.ColourLevel{it23}));
end
specsSNAP.Filter   = char(mmc.getProperty('TIFilterBlock1','Label'));
specsSNAP.Gain     = char(mmc.getProperty('Andor','Gain'));
specsSNAP.Exposure = char(mmc.getProperty('Andor','Exposure'));
specsSNAP.PerfectFocus = char(mmc.getProperty('TIPFSStatus','State'));

end