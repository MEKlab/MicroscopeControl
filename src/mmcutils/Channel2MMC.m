function [Filter,Illumination] = Channel2MMC(channel)

% Illumination:
% Brigthfield: 9,Cyan 2, Green 3
% Filters:
% '5-TRITC','3-FITC','5-TRITC'
switch channel
  case 'Brighfield'
    Filter = '5-TRITC';
    Illumination = '';
  case 'GFP'
    Filter = '3-FITC';
    Illumination = 'Cyan';
  case 'mCherry'
    Filter = '5-TRITC';
    Illumination = 'Green';
  case 'DAPI'
    Filter = '1-DAPI';
    Illumination = 'Violet';
end
    