function [refps,scores,imgs] = findfftfocus(img,nfft,minf,maxf)

% Returns:
% refps -> 1x3 
% scores
% imgs

% distance matrix
[dmat] = makedistancematrix(nfft);
% actual positions of the matrix that we are interested
[pos]  = makecylindermaskpos(dmat,nfft*minf,nfft*maxf);
% fft2 centered
ffts = myff2(img,nfft);
% Absolute value score
score1 = computeZabsScore(ffts,dmat,pos);
% This will be used a reference for phase score
[~,refp1] = max(score1);
% Phase score
score2 = computeZangScore(ffts,dmat,pos,refp1);
% Product
score3 = score2.*score1;

[~,refp2] = max(score3);
% Best focus position
p1 = min(refp1,refp2);
p2 = max(refp1,refp2);
[~,focus] = min(score1(p1:p2));
focus = focus+p1-1;

refps = [p1,focus,p2]';
scores = [score1;score2];
imgs(:,:,1) = img(:,:,p1); 
imgs(:,:,2) = img(:,:,focus); 
imgs(:,:,3) = img(:,:,p2); 

end