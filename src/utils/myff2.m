function [ffts] = myff2(img,nfft)

[~,~,nstacks] = size(img);
if(nstacks==1)
    ffts = fft2(img,nfft,nfft);
else
    ffts = zeros(nfft,nfft,nstacks);
    for n = 1:nstacks
        %img(:,:,n) = imfilter(img(:,:,n),fspecial('gaussian',5,1),'replicate');
        ffts(:,:,n) = fftshift(fft2(img(:,:,n),nfft,nfft));
    end
end

end