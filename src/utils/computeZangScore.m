function [scores,refangles] = computeZangScore(ffts,dmat,pos,ref)

[~,~,nstacks] = size(ffts);

scores = zeros(1,nstacks);
refangles = angle(ffts(:,:,ref));
for n = 1:nstacks
    v = abs(ffts(:,:,n));
    vt = sum(sum(v));
    va = abs(angle(ffts(:,:,n))-refangles);
    %scores(n) = circ_mean(va(pos));
    scores(n) = circ_mean(va(pos),v(pos));
    %scores(n) = circ_mean(va(pos),v(pos)./vt);
end

end