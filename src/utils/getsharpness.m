function [sharpness] = getsharpness(img)
    sharpness = mean2(abs(imgradient(img)));
end