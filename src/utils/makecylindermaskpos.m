function [pos] = makecylindermaskpos(dmat,mind,maxd)

[nx,ny] = size(dmat);

[nx2 ny2] = meshgrid(1:nx,1:ny);

mask1 = (nx2 - nx/2).^2 + (ny2 - ny/2).^2 <= mind.^2;
mask2 = (nx2 - nx/2).^2 + (ny2 - ny/2).^2 <= maxd.^2;

pos = find((mask2-mask1)==1);

end