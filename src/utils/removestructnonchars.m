function [strout] = removestructnonchars(str)
    badchars = '\s|\(|\)|\||\:|\.|\-';
    strout = regexprep(str,badchars,'_');
end