function [sharpness] = getsharpness2(img)
    sharpness = mean2(imgradient(img));
end