function [dmat] = makedistancematrix(n)

xmat = repmat([1:n]-n/2,n,1);
ymat = repmat([1:n]'-n/2,1,n);
dmat = sqrt(xmat.^2+ymat.^2); 

end