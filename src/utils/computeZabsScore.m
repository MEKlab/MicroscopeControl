function [scores] = computeZabsScore(ffts,dmat,pos)

[~,~,nstacks] = size(ffts);

scores = zeros(1,nstacks);
for n = 1:nstacks
    v = abs(ffts(:,:,n));
    vt = sum(sum(v));
    %sv = dmat(pos).*v(pos)./vt;
    sv = dmat(pos).*v(pos);
    scores(n) = mean(sv);
end

end