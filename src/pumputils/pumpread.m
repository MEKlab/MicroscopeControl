function [ret] = pumpread(pumpserial,query,n)

dumplastbyte(pumpserial); % just in case

fprintf(pumpserial,sprintf(query));
while(~pumpserial.BytesAvailable) 
end
ret = fread(pumpserial,n);

dumplastbyte(pumpserial); % just in case

end