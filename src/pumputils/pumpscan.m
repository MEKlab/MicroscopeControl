function [ret] = pumpscan(pumpserial,query)

dumplastbyte(pumpserial); % just in case

fprintf(pumpserial,sprintf(query));
ret = fscanf(pumpserial);

dumplastbyte(pumpserial); % just in case

end