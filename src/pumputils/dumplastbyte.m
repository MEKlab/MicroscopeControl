function [] = dumplastbyte(pumpserial)

if pumpserial.BytesAvailable
    dump = char(fread(pumpserial,pumpserial.BytesAvailable)'); % Just to get rid ofthe last byte
end

end