function [bool] = ispumpruning(pumpserial)

ret = 42; % star... means it is busy (I think)

while(ret==42)
    ret = pumpread(pumpserial,'1E\n',1);
end

if(ret==43)
    bool = 0;
elseif(ret==45)
    bool = 1;
else
    bool = NaN;
end

end