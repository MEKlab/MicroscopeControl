function [] = setpumpspeed(pumpserial,value)

% value is bewteen 0 and 1

str = ['1S' num2str(1000*value,'%05.f') '\n'];

fprintf(pumpserial,sprintf(str));

while(pumpserial.BytesAvailable==0) 
    pause(.05); 
end
dumplastbyte(pumpserial);

end