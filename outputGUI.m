function [] = outputGUI()

    lastversion = '20170616';
    
    addpath(genpath('./src/'));
    
    gui = struct;
    gui.myblack = 0.2*[1,1,1];
    gui.mywhite = 0.99*[1,1,1];
    gui.defbkgcol = [0.94,0.94,0.94];
    gui.busycolor = 'yellow';
    gui.OFFcolor  = 'red';
    gui.ONcolor   = 'green';
    gui.handleMap = struct;
    gui.folder    = '';
    %XYadq = struct;
    date = getdate();
    
    gui.figX = 1300;
    gui.figY = 720;
    
    Xoffset  = 0.005*gui.figX;
    Yoffset  = 0.005*gui.figY;
    dw1      = 5;
    bheight1 = 40;
    bwidth1   = 91;
    
    lastc = 0;
    % https://uk.mathworks.com/matlabcentral/answers/96816-how-do-i-change-the-default-background-color-of-all-figure-objects-created-in-matlab
    prevcolor = get(0,'defaultfigurecolor');
    set(0,'defaultfigurecolor',gui.myblack);
    fig = figure('Visible','off','MenuBar','none',...
                 'Color',gui.myblack,...
                 'Position',[200,200,gui.figX,gui.figY]);
    set(0,'defaultfigurecolor',prevcolor);
    %get(fig)
    % custom pointer? https://uk.mathworks.com/help/matlab/ref/figure-properties.html#zmw57dd0e277372
    
    p1dx = 0.29-0.005;
    p1dy = 1-0.005;

    gui.tabgp(1) = uitabgroup(fig,'Position',[0.005,0.005,1-p1dx,p1dy]);
    gui.tabs(1) = uitab(gui.tabgp(1),'Title','Viewer');    

    gui.tabgp(2) = uitabgroup(fig,'Position',[1-p1dx+0.005,0.1+0.005,p1dx,p1dy*0.9]);
    gui.tabs(2) = uitab(gui.tabgp(2),'Title','LOG-1');
    gui.tabs(3) = uitab(gui.tabgp(2),'Title','LOG-2');
    gui.tabs(4) = uitab(gui.tabgp(2),'Title','Other');
    
    gui.panels(1) = uipanel(fig,'Title','Main','FontSize',12,...
                            'Position',[1-p1dx+0.005,0.005,p1dx,p1dy*0.1]);

    %% Main
    labelstrs = {'Load','Save','Quit'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(1),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[dw1+(i-1)*(dw1+bwidth1+20),dw1,bwidth1+20,bheight1]);
    end
    set(gui.controls(1+lastc),'Callback',@Hloadbutton_Callback);
    set(gui.controls(2+lastc),'Callback',@Hsavebutton_Callback);
    set(gui.controls(3+lastc),'Callback',@Hquitbutton_Callback);
    lastc = 3+lastc;
    
    %% Main view
    % name
    % ST
    % XY
    % IT
    gui.panels(2) = uipanel(gui.tabs(1),'Title','Navigation','FontSize',12,...
                            'Position',[0.005,p1dy*0.85+0.005,0.7-0.005,0.15]);
    gui.panels(3) = uipanel(gui.tabs(1),'Title','Image','FontSize',12,...
                            'Position',[0.005,0.005,0.7-0.005,0.85-0.005]);    
    
    gui.panels(4) = uipanel(gui.tabs(1),'Title','Options','FontSize',12,...
                            'Position',[0.7+0.005,0.005,0.3-0.005,1-0.005]);    

    gui.controls(1+lastc) = uicontrol(gui.panels(2),'Style','text',...
                                      'String','Load a folder with data',...
                                      'HorizontalAlignment','center',...
                                      'FontSize',14,...
                                      'Position',[dw1,bheight1-5,4*bwidth1,bheight1]);
    
    gui.controls(2+lastc) = uicontrol(gui.panels(2),'Style','text',...
                                      'String','CHN',...
                                      'HorizontalAlignment','left',...
                                      'FontSize',12,...
                                      'Position',[(3-1)*(dw1+2.2*bwidth1)-10,bheight1-5,40,bheight1*0.75]);
    gui.controls(3+lastc) = uicontrol(gui.panels(2),'Style','popup','String',{''},...
                                      'Callback',@updateIMGpop_Callback,...
                                      'Position',[25+dw1+(3-1)*(dw1+2.2*bwidth1),bheight1,1.9*bwidth1,bheight1*0.75]);
    gui.handleMap.('PrefixTitle') = 1+lastc;
    gui.handleMap.('CHpopup')     = 3+lastc;
    lastc = 3+lastc;
    
    labelstrs = {'ST','XY','IT'};
    typestrs = {'text','text','text'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(2),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Position',[dw1+(i-1)*(dw1+2.2*bwidth1),dw1-5,25,bheight1*0.75]);
    end
    lastc = 3+lastc;
    
    labelstrs = {{''},{''},{''}};
    typestrs = {'popup','popup','popup'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(2),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Callback',@updateIMGpop_Callback,...
                                          'Position',[25+dw1+(i-1)*(dw1+2.2*bwidth1),dw1,1.9*bwidth1,bheight1*0.75]);
    end
    gui.handleMap.('STpopup') = 1+lastc;
    gui.handleMap.('XYpopup') = 2+lastc;
    gui.handleMap.('ITpopup') = 3+lastc;

    lastc = 3+lastc;
% $$$     pos = get(gui.controls(1+lastc),'Position');
% $$$     pos(2) = -7;
% $$$     pos(4) = bheight1;
% $$$     set(gui.controls(1+lastc),'Position',pos);
    
    gui.axes(1)  = axes('Position',[0,0.035,1,0.95],'Parent',gui.panels(3));
    img = rand(512,512)*100;
    imshow(img,[],'Parent',gui.axes(1));
    gui.handleMap.imageAxes = 1;
    
    labelstrs = {'IT>','IT<'};
    typestrs = {'pushbutton','pushbutton'};
    for i = [1:2]
        gui.controls(i+lastc) = uicontrol(gui.panels(3),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Position',[590,100+i*(25+5),40,25]);
    end
    set(gui.controls(1+lastc),'Callback',@moveUpIT);
    set(gui.controls(2+lastc),'Callback',@moveDwIT);
    lastc = 2+lastc;
    
    labelstrs = {'CH>','CH<'};
    typestrs = {'pushbutton','pushbutton'};
    for i = [1:2]
        gui.controls(i+lastc) = uicontrol(gui.panels(3),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Position',[590,200+i*(25+5),40,25]);
    end
    set(gui.controls(1+lastc),'Callback',@moveUpCH);
    set(gui.controls(2+lastc),'Callback',@moveDwCH);
    lastc = 2+lastc;
        
    labelstrs = {'XY>','XY<'};
    typestrs = {'pushbutton','pushbutton'};
    for i = [1:2]
        gui.controls(i+lastc) = uicontrol(gui.panels(3),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Position',[590,300+i*(25+5),40,25]);
    end

    set(gui.controls(1+lastc),'Callback',@moveUpXY);
    set(gui.controls(2+lastc),'Callback',@moveDwXY);
    lastc = 2+lastc;

    for t = 1:4
        %set(gui.tabs(t),'ForegroundColor',[0.9,0.4,0]);%[0.6,0,0]
        set(gui.tabs(t),'BackgroundColor',gui.myblack);
    end
    for p = 1:length(gui.panels)
        set(gui.panels(p),'BackgroundColor',gui.myblack);
        % orange
        set(gui.panels(p),'ForegroundColor',[1,0.5,0.25]); %gui.mywhite
    end
    for c = 1:length(gui.controls)
        if(strcmp(get(gui.controls(c),'Style'),'text'))
            set(gui.controls(c),'BackgroundColor',gui.myblack);
            set(gui.controls(c),'ForegroundColor',gui.mywhite);
        else
            set(gui.controls(c),'BackgroundColor',gui.mywhite);
            set(gui.controls(c),'ForegroundColor',gui.myblack);
        end
    end
    
    set(fig,'Name','Output Viewer');
    set(fig,'Visible','on');
    movegui(fig,'center');
    
    function Hquitbutton_Callback(~,~)
        close(fig);
    end

    function [] = Hloadbutton_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor);
        foldername = uigetdir();
        if(ischar(foldername))
            foldername = [foldername,'/'];
            imgfiles = dir([foldername,'IMG-*.mat']);
            logfiles = dir([foldername,'*LOG-*.mat']);
            %loadfiles(imgfiles,foldername,2);
            [prefix,steps,xys,chns,its] = parseIMGfiles(imgfiles);            
            gui.folder = foldername;
            set(gui.controls(gui.handleMap.('PrefixTitle')),'String',prefix);
            if(~isempty(steps))
                set(gui.controls(gui.handleMap.('STpopup')),'String',steps);
            end
            set(gui.controls(gui.handleMap.('XYpopup')),'String',xys);
            set(gui.controls(gui.handleMap.('ITpopup')),'String',its);
            set(gui.controls(gui.handleMap.('CHpopup')),'String',chns);
            updateIMGpop_Callback();
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [prefix,steps,xys,chns,its] = parseIMGfiles(imgfiles)
        Sprefixes  = struct;
        Ssteps     = struct;
        Sxys       = struct;
        Schns      = struct;
        Sits       = struct;
        for n = 1:length(imgfiles)
            name = imgfiles(n).name;
            if(isempty(strfind(name,'_ST-')))
                % Other
                xyp = strfind(name,'_XY-');
                chp = strfind(name,'_CHN-');
                itp = strfind(name,'_IT-');
                prefix  = name(5:xyp-1);
                xy      = strrep(name(xyp+4:chp-1),'-','_');
                chn     = name(chp+5:itp-1);
                it      = name(itp+4:end-4);
                Sprefixes.(['T',prefix]) = 1;
                Sxys.(['T',xy])          = 1;
                Schns.(['T',chn])        = 1;
                Sits.(['T',it])          = 1;
            else
                % MACS
                stp = strfind(name,'_ST-');
                xyp = strfind(name,'_XY-');
                chp = strfind(name,'_CHN-');
                itp = strfind(name,'_IT-');
                prefix  = name(5:stp-1);
                step    = name(stp+4:xyp-1);
                xy      = name(xyp+4:chp-1);
                chn     = name(chp+5:itp-1);
                it      = name(itp+4:end-4);
                Sprefixes.(['T',prefix]) = 1;
                Ssteps.(['T',step])      = 1;
                Sxys.(['T',xy])          = 1;
                Schns.(['T',chn])        = 1;
                Sits.(['T',it])          = 1;
            end
        end
        prefixes  = fieldnames(Sprefixes);
        steps     = fieldnames(Ssteps);
        xys       = fieldnames(Sxys);
        chns      = fieldnames(Schns);
        its       = fieldnames(Sits);
        if(length(prefixes)>1)
            display('Ooops!');
        else
            prefix = prefixes{1}(2:end);
        end
        for n = 1:length(steps)
            steps{n} = steps{n}(2:end);
        end
        for n = 1:length(xys)
            xys{n} = xys{n}(2:end);
            %xys{n} = strrep(xys{n}(2:end),'_','-');
        end
        for n = 1:length(chns)
            chns{n} = chns{n}(2:end);
        end
        for n = 1:length(its)
            its{n} = its{n}(2:end);
        end
    end
    function [] = updateIMGpop_Callback(~,~)
        prefix = get(gui.controls(gui.handleMap.('PrefixTitle')),'String');
        steps  = get(gui.controls(gui.handleMap.('STpopup')),'String');
        xys    = get(gui.controls(gui.handleMap.('XYpopup')),'String');
        its    = get(gui.controls(gui.handleMap.('ITpopup')),'String');
        chns   = get(gui.controls(gui.handleMap.('CHpopup')),'String');
        nsteps  = get(gui.controls(gui.handleMap.('STpopup')),'Value');
        nxys    = get(gui.controls(gui.handleMap.('XYpopup')),'Value');
        nits    = get(gui.controls(gui.handleMap.('ITpopup')),'Value');
        nchns   = get(gui.controls(gui.handleMap.('CHpopup')),'Value');
        if(~strcmp(steps{1},''))
            filename = [gui.folder,...
                        'IMG-',prefix,...
                        '_ST-',steps{nsteps},...
                        '_XY-',xys{nxys},...
                        '_CHN-',chns{nchns},...
                        '_IT-',its{nits},...
                        '.mat'];
        else
            filename = [gui.folder,...
                        'IMG-',prefix,...
                        '_XY-',xys{nxys},...
                        '_CHN-',chns{nchns},...
                        '_IT-',its{nits},...
                        '.mat'];
        end
        if(exist(filename))
            temp = load(filename);
            imshow(temp.img,[],'Parent',gui.axes(gui.handleMap.imageAxes));
        end
    end
    function [] = moveUpIT(~,~)
        its    = get(gui.controls(gui.handleMap.('ITpopup')),'String');
        nits   = get(gui.controls(gui.handleMap.('ITpopup')),'Value');
        if(~(nits==length(its)))
            set(gui.controls(gui.handleMap.('ITpopup')),'Value',nits+1);
        end
        updateIMGpop_Callback();
    end
    function [] = moveDwIT(~,~)
        its    = get(gui.controls(gui.handleMap.('ITpopup')),'String');
        nits    = get(gui.controls(gui.handleMap.('ITpopup')),'Value');
        if(~(nits==1))
            set(gui.controls(gui.handleMap.('ITpopup')),'Value',nits-1);
        end
        updateIMGpop_Callback();
    end
    function [] = moveUpCH(~,~)
        chns   = get(gui.controls(gui.handleMap.('CHpopup')),'String');
        nchns  = get(gui.controls(gui.handleMap.('CHpopup')),'Value');
        if(~(nchns==length(chns)))
            set(gui.controls(gui.handleMap.('CHpopup')),'Value',nchns+1);
        end
        updateIMGpop_Callback();
    end
    function [] = moveDwCH(~,~)
        chns   = get(gui.controls(gui.handleMap.('CHpopup')),'String');
        nchns  = get(gui.controls(gui.handleMap.('CHpopup')),'Value');
        if(~(nchns==1))
            set(gui.controls(gui.handleMap.('CHpopup')),'Value',nchns-1);
        end
        updateIMGpop_Callback();
    end
    function [] = moveUpXY(~,~)
        xys    = get(gui.controls(gui.handleMap.('XYpopup')),'String');
        nxys   = get(gui.controls(gui.handleMap.('XYpopup')),'Value');
        if(~(nxys==length(xys)))
            set(gui.controls(gui.handleMap.('XYpopup')),'Value',nxys+1);
        end
        updateIMGpop_Callback();
    end
    function [] = moveDwXY(~,~)
        xys    = get(gui.controls(gui.handleMap.('XYpopup')),'String');
        nxys    = get(gui.controls(gui.handleMap.('XYpopup')),'Value');
        if(~(nxys==1))
            set(gui.controls(gui.handleMap.('XYpopup')),'Value',nxys-1);
        end
        updateIMGpop_Callback();
    end
end